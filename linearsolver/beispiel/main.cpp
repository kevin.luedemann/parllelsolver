//notwendige includes
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <cmath>
#include <string>
using namespace std;

//++++++++++++++++++++++++++++++++++++++++
//Parameter zum einstellen der Berechnung
char RUN_NAME[]	= "felder";
double dt	= 0.0002;
int NX		= 30;
int NY		= NX;
double dx	= 1.0/(double)NX;
double dy	= 1.0/(double)NY;
//----------------------------------------

//++++++++++++++++++++++++++++++++++++++++
//Funktionsdeklarationen
//bei allen Vektorfunktionen mit zwei vectoren wird immer der erste uebershrieben
//bei Matrix vector operationen wird immer der vektor ueberschrieben
void setze_IC(double **T);
void setze_N(int N);
void matrix_vektor_mul(double **A, double *b, int n);
void matrix_ober_mul(double **A, double *b, int n);
void matrix_print(double **M, int n);
void matrix_print_k(double **M, int n);
void vector_print(double *x, int n);
void vector_print_k(double *x, int n);
void vector_addition(double *x, double *b, int n);
void vector_subtraktion(double *x, double *b, int n);
void vector_copy(double *a, double *b, int n);
void vector_scalar(double *a, double w, int n);
void loese_vorwaerts(double **LD, double *x, int n);
double **build_matrix(double Pe, int n);
double *build_vector(double **T,  double Pe,int n);
void get_vector(double *v, double **T, double Pe, int n);
double vector_betrag(double *r, int n);
int SOR(double **M, double *b, int n, double toll, double ww, bool find_best);
void zeitschritt_implizit(double **T, double Pe, double omega, bool alle_omega);
double fehler_loesung(double **T, double **T_stat);
double Berechnung_implizit(double T_MAX, double Pe, double omega, bool alle_omega);
void ausgabe_feld(double **T, FILE *out, double zeit, double Pe);
//----------------------------------------
//im Programm wird zur Erstellung von Matrizen die Funktion aus 
//"Numerical Recipes in c"
//Second Edition
//Cambridge University Press
//ISBN 0521431085
#define NR_END 1
#define FREE_ARG char*
double **dmatrix(long nrl, long nrh, long ncl, long nch)
/* allocate a double matrix with subscript range m[nrl..nrh][ncl..nch] */
{
   long i, nrow=nrh-nrl+1,ncol=nch-ncl+1;
   double **m;

   /* allocate pointers to rows */
   m=(double **) malloc((size_t)((nrow+NR_END)*sizeof(double*)));
   m += NR_END;
   m -= nrl;

   /* allocate rows and set pointers to them */
   m[nrl]=(double *) malloc((size_t)((nrow*ncol+NR_END)*sizeof(double)));
   m[nrl] += NR_END;
   m[nrl] -= ncl;

   for(i=nrl+1;i<=nrh;i++) m[i]=m[i-1]+ncol;

   /* return pointer to array of pointers to rows */
   return m;
}
void free_dmatrix(double **m, long nrl, long nrh, long ncl, long nch)
/* free a double matrix allocated by dmatrix() */
{
   free((FREE_ARG) (m[nrl]+ncl-NR_END));
   free((FREE_ARG) (m+nrl-NR_END));
}


int main(int argc, char *argv[]){
	printf("BTCS\n");
	setze_N(30);
	dt = 100.0;
	
	//starte die Berechnung mit dt = T_MAX (erster Parameter)
	//mit einer Peclet-Zahl von Pe=10 (zweiter parameter)
	//mit optimale omega (dritter parameter)
	//mit der iteration ueber alle omega (letzer parameter)
	Berechnung_implizit(0.0,10.0,1.58,false);
	
	return 0;
}

//Funktion zum seten der IC
void setze_IC(double **T){
	int i,j;
	for(i=0;i<=NX;i++){
		for(j=0;j<=NY;j++){
			T[i][j]			= j*dy; 
		}
	}
}
//Funktion zum setzten der Parameter bei aenderung von N
void setze_N(int N){
	NX=N;	NY=NX;
	dx	= 1.0/(double)NX;
	dy	= 1.0/(double)NY;
}

double Berechnung_implizit(double T_MAX, double Pe, double omega, bool alle_omega){
//Funktion zur Berechnung implizit
//bei der Berechung ist es sinncoll T_MAX auf 0 und dt=T_MAX zu waehlen,
//dann wird das Programm nur einmal durchlaufen
	double Fehler_berechung = 0.0;

	char ausgabe_name[50];
	sprintf(ausgabe_name,"%s_implizit_%.0e.dat",RUN_NAME,Pe);
	FILE *ausgabe;
	ausgabe = fopen(ausgabe_name, "w+");

	double **T;	T = dmatrix(0,NX,0,NY);
	setze_IC(T);

	double zeit = 0.0;
	while(zeit<=T_MAX){
		zeitschritt_implizit(T,Pe,omega,alle_omega);
		zeit += dt;
	}

	ausgabe_feld(T,ausgabe,zeit,Pe);
	fclose(ausgabe);
	free_dmatrix(T,0,NX,0,NY);
	return Fehler_berechung;

}

//Funktion zur Ausgabe des Feldes in die Ausgabedatei mit der jeweiligen Zeit und der Peclet Zahl
//die Ausgabe erfolgt im Matrixformat, welches von gnuplot gelesen werden kann
void ausgabe_feld(double **T, FILE *out, double zeit, double Pe){
	fprintf(out,"#Zeit=%e\tPe=%f\n",zeit,Pe);
	int i,j;
	for(i=0;i<=NX;i++){
		for(j=0;j<=NY;j++){
			fprintf(out,"%e ",T[i][j]);
		}
		fprintf(out,"\n");
	}
	fprintf(out,"\n\n");
}

void zeitschritt_implizit(double **T, double Pe, double omega, bool alle_omega){
//Funktion, die einen Zeitschritt ausfuehrt
//die Parameter werden uebergegben
	double *T_vec;	
	double **M;
	//erstellen von Matrix und Vektor
	M = build_matrix(Pe,NX);
	T_vec = build_vector(T,Pe,NX);
	int steps = SOR(M,T_vec,(NX+1)*(NX+1)-1,1e-4,omega,alle_omega);
	printf("#steps=%d\n",steps);
	//zurueckholen der Werte
	get_vector(T_vec,T,Pe,NX);
	free(T_vec);
	free_dmatrix(M,0,(NX+1)*(NX+1)-1,0,(NX+1)*(NX+1)-1);
}

int SOR(double **M, double *b, int n, double toll, double ww, bool find_best){
//Gaus-Seidel-Verfahren mit Ueberrelaxation
//solle find_best=true sein, werden alle omega zwischen 0.01 und 2.0 mit 0.01 durchgelaufen
//das angegebene omega wird in jedem fall als letzten berechnet
//die interation ueber omega wird in eiener Datei gespeichert
	int i,steps=0;
	double w = ww;
	double Fehler;
	double *x; x = (double *)malloc((n+2)*sizeof(double));
	double *r; r = (double *)malloc((n+2)*sizeof(double));
	double *x_alt; x_alt = (double *)malloc((n+2)*sizeof(double));

	if(find_best){
		char ausgabe_name[50];
		sprintf(ausgabe_name,"%s_w_analyse_%d_%.0e.dat",RUN_NAME,NX,toll);
		FILE *ausgabe;
		ausgabe = fopen(ausgabe_name, "w+");
		w = 1.0;
		while(w<=1.7){
		steps = 0;
		for(i=0;i<=n;i++){
				x[i] = 1.0;
		}
	
		vector_copy(x_alt,x,n);
		matrix_vektor_mul(M,x_alt,n);
		vector_subtraktion(x_alt,b,n);
		Fehler = vector_betrag(x_alt,n);
		//Beenden, sobald Fehler kleiner tolleranz, steps maximal oder Fehler divergiert
		while(Fehler>toll && steps < 1e4 && !isnan(Fehler) && !isinf(Fehler)){
			loese_vorwaerts(M,x_alt,n);
			vector_scalar(x_alt,w,n);
			vector_subtraktion(x,x_alt,n);
			//berechne rn
			vector_copy(x_alt,x,n);
			matrix_vektor_mul(M,x_alt,n);
			vector_subtraktion(x_alt,b,n);
			Fehler = vector_betrag(x_alt,n);
				
		steps++;	
		}
	
		printf("%.02e\t%03d\t%+e\n",w,steps,Fehler);
		fprintf(ausgabe,"%.02e\t%03d\t%+e\n",w,steps,Fehler);
		w += 0.1;
		}
		w = ww;
		fclose(ausgabe);
	}

	//eigentliche Berechnung mit dem uebergebenen omega
	steps = 0;
	for(i=0;i<=n;i++){
			x[i] = 1.0;
	}

	vector_copy(x_alt,x,n);
	matrix_vektor_mul(M,x_alt,n);
	vector_subtraktion(x_alt,b,n);
	Fehler = vector_betrag(x_alt,n);
	while(Fehler>toll && steps < 1e4 && !isnan(Fehler) && !isinf(Fehler)){
		loese_vorwaerts(M,x_alt,n);
		vector_scalar(x_alt,w,n);
		vector_subtraktion(x,x_alt,n);
		//berechne rn
		vector_copy(x_alt,x,n);
		matrix_vektor_mul(M,x_alt,n);
		vector_subtraktion(x_alt,b,n);
		Fehler = vector_betrag(x_alt,n);
			
		steps++;	
	}
	
	printf("%.02e\t%03d\t%+e\n",w,steps,Fehler);
	for(i=0;i<=n;i++){
		b[i] = x[i];
	}
	free(x_alt);
	free(x);
	free(r);
	
	return steps;
}

//Matrix stuff
void matrix_ober_mul(double **A, double *b, int n){
//multipliziere nur das obere dreieck der Matrix
	int i,j;
	double *x; x = (double *)malloc((n+1)*sizeof(double));
	for(i=0;i<=n;i++){
		x[i] = 0.0;
	}
	for(i=0;i<=n;i++){
		for(j=i+1;j<=n;j++){
			x[i] += A[i][j]*b[j];
		}
	}
	for(i=0;i<=n;i++){
		b[i] = x[i];
	}
	free(x);
}

void matrix_print(double **M, int n){
//ausgabe einer Matrix
	int i,j;
	for(i=0;i<=n;i++){
		for(j=0;j<=n;j++){
			printf("%+.04e ",M[i][j]);
		}
		printf("\n");
	}
}

void matrix_print_k(double **M, int n){
//ausgabe einer Matrix fuer implizit euler
	int i,j;
	for(i=0;i<n;i++){
		for(j=0;j<n;j++){
			printf("%+.04e ",M[i][j]);
		}
		printf("\n");
	}
}

void vector_print(double *x, int n){
//ausgabe eines vektors
	int i;
	for(i=0;i<=n;i++){
		printf("%+e\n",x[i]);
	}
}

void vector_print_k(double *x, int n){
//ausgabe eines Vektors aus implizit euler
	int i;
	for(i=0;i<n;i++){
		printf("%+e\n",x[i]);
	}
}

void loese_vorwaerts(double **LD, double *x, int n){
//Vorwaertssubstitution fuer das Loesen von (L+D)^-1
	int i,j;
	double *v; v = (double *)malloc((n+1)*sizeof(double));
	v[0] = x[0]/LD[0][0];
	for(i=1;i<=n;i++){
		v[i] = x[i];
		for(j=0;j<i;j++){
			v[i] -= v[j]*LD[i][j];
		}
		v[i] /= LD[i][i];
	}
	for(i=0;i<=n;i++){
		x[i] = v[i];
	}
	free(v);
}

void vector_scalar(double *a, double w, int n){
//mutipliziere vector mit scalar
	int i;
	for(i=0;i<=n;i++){
		a[i] = a[i] * w;
	}
}

void vector_copy(double *a, double *b, int n){
//kopiere Vektor 2 in 1
	int i;
	for(i=0;i<=n;i++){
		a[i] = b[i];
	}
}

void vector_addition(double *x, double *b, int n){
//addieren zwei Vektoren
	int i;
	for(i=0;i<=n;i++){
		x[i] += b[i];
	}
}

void vector_subtraktion(double *x, double *b, int n){
//subtrahiere zwei Vektoren
	int i;
	for(i=0;i<=n;i++){
		x[i] = x[i] - b[i];
	}
}

double vector_betrag(double *r, int n){
//berechne Betrag des Vektors
	int i;
	double summe = 0.0;
	for(i=0;i<=n;i++){
		summe += r[i]*r[i];
	}
	return sqrt(summe);
}

double **build_matrix(double Pe, int n){
//erstelle die matrix fuer implizit euler
//die Matrix hat die indexierung aufgrund des Verhaltens der Indizes
//j ist der Schnell variirende und i der langsame
	int i,j;
	double s = dt*Pe/(2.0*dx);
	double k = dt/(dx*dx);
	int na = n+1;
	int nn = na*na;
	double **M; M = dmatrix(0,nn-1,0,nn-1);
	for(i=0;i<nn;i++){
		for(j=0;j<0;j++){
			M[i][j] = 0.0;
		}
	}
	//Berechnung der Geschwindigkeits Matrizen
	double **v0_x; v0_x = dmatrix(0,n,0,n);
	double **v0_y; v0_y = dmatrix(0,n,0,n);
	for(i=0;i<=n;i++){
		for(j=0;j<=n;j++){
			v0_x[i][j] = M_PI*sin(2.0*M_PI*i*dx)*cos(M_PI*j*dy);
			v0_y[i][j] = -2.0*M_PI*cos(2.0*M_PI*i*dx)*sin(M_PI*j*dy);
		}
	}

	//die Eintraege sind dierekt aus dem Verfahren abgelesen
	//die Randbedingungen sind bereits beachtet
	for(i=0;i<=n;i++){
		for(j=0;j<=n;j++){
			//hauptdiagonale
			M[i*na+j][i*na+j] = 1.0+4.0*k;
			if(j==0 || j==n){
				//dirichlet Raender
				M[i*na+j][i*na+j] = 1.0;
			}
			else{
				//Nebendiagonale der Diagonalbloecke
				M[i*na+j][i*na+j-1] = -(s*v0_y[i][j]+k);
				M[i*na+j][i*na+j+1] =  (s*v0_y[i][j]-k);
			}
			if(i==0 && j != n && j!= 0){
				//Neumann-Raender
				M[i*na+j][(i+1)*na+j] = -2.0*k;
			}
			else if(i==n && j != n && j!= 0){
				//Neumann-Raender
				M[i*na+j][(i-1)*na+j] = -2.0*k;
			}
			else if(j!= 0 && j!= n){
				//Nebendiagonalbloecke
				M[i*na+j][(i-1)*na+j] = -(s*v0_x[i][j]+k);
				M[i*na+j][(i+1)*na+j] =  (s*v0_x[i][j]-k);
			}
		}
	}

	free_dmatrix(v0_x,0,n,0,n);	
	free_dmatrix(v0_y,0,n,0,n);

	return M;
}

double *build_vector(double **T, double Pe, int n){
	//erstellen des Vektors mit den Eintraegen aus dem Temperaturfeld
	//der Vektor hat die indexierung aufgrund des Verhaltens der Indizes
	//j ist der Schnell variirende und i der langsame
	int i,j;
	int nn = (n+1)*(n+1);
	double *v; v = (double *)malloc((nn-1)*sizeof(double));
	for(i=0;i<=n;i++){
		for(j=0;j<=n;j++){
			v[i*(n+1)+j] = T[i][j];
		}
	}
	return v;
}

void get_vector(double *v, double **T, double Pe, int n){
	//erzeuge aus dem Vektor wieder das Temperaturfeld
	int i,j;
	for(i=0;i<=n;i++){
		for(j=0;j<=n;j++){
			T[i][j] = v[i*(n+1)+j]; 
		}
	}
}

void matrix_vektor_mul(double **A, double *b, int n){
//Matrix Vektor multiplikation
	int i,j;
	double *x; x = (double *)malloc((n+1)*sizeof(double));
	for(i=0;i<=n;i++){
		x[i] = 0.0;
	}
	for(i=0;i<=n;i++){
		for(j=0;j<=n;j++){
			x[i] += A[i][j]*b[j];
		}
	}
	for(i=0;i<=n;i++){
		b[i] = x[i];
	}
	free(x);
}

