#!/usr/bin/python
"""
Beispielsprogramm fuer eine Anwendung der Module auf ein echtes Problem.
Hier wird eine zweidimensionale Diffusions-Advektions-Gleichung mit inversem Zeitschritt geloest.
Das Programm erstellt die noetigen Matrizen und Vektoren.
Anschliessend werden diese mit den einzelnen Modulen nacheinander berechnet.
Es gibt die Moeglichkeit eine graphische Ausgabe durchzufuehren, oder alle Loeser zu vergleichen.

Author: Kevin Luedemann
EMail: kevin.luedemann@stud.uni-goettingen.de
"""

import numpy as np
#nur fuer die Plots noetig
import matplotlib as mp
#Abfrage ob ein Graphisches Backend gewaehlt wurde oder nicht
#Fals Nein wird es ausgewahlt
backend =  mp.get_backend()
if backend == 'agg':
	mp.use('TkAgg')
import matplotlib.pyplot as plt
import scipy as sp
from pylab import *
import time as ti

#Hier wird die Matrix aus dem Schema fuer den inversen Zeitschritt erstellt
#Es wird ein dense Schema verwendet
#Dies kann aber durch konvertierung geandert werden
def erstelle_Matrix(Pe=14, dt=100.0, N=10):
    dx  = 1.0/float(N+1)
    s   = dt*Pe/(2.0*dx)
    k   = dt/(dx*dx)
    nn  = (N+1)*(N+1)
    na  = N+1
    A   = np.zeros((nn,nn))
    x   = np.linspace(0,1,na)
    y   = np.linspace(0,1,na)

    #Geschwidigkeitsvektoren (siehe Hausarbeit)
    vx  = np.outer(sin(2.0*pi*x),cos(pi*y))*pi
    vy  = -1*np.outer(cos(2.0*pi*x),sin(pi*y))*2.0*pi

    #Iteration ueber die Matrixelemente
    #kann uber stencel optimiert werden
    #fuer Schema siehe Hausarbeit
    for i in range(na):
        for j in range(na):
            A[i*na+j][i*na+j] = 1.0*4.0*k
            if j==0 or j==N:
                A[i*na+j][i*na+j] = 1.0
            else:
                A[i*na+j][i*na+j-1] = -(s*vy[i][j]+k)
                A[i*na+j][i*na+j+1] = s*vy[i][j]-k
            if i==0 and j != 0 and j != N:
                A[i*na+j][(i+1)*na+j] = -2.0*k
            elif i==N and j != 0 and j != N:
                A[i*na+j][(i-1)*na+j] = -2.0*k
            elif j != 0 and j != N:
                A[i*na+j][(i-1)*na+j] = -(s*vx[i][j]+k)
                A[i*na+j][(i+1)*na+j] = (s*vx[i][j]-k)
    return A

#Mit dieser Funktion wird der Vektor fuer die Berechnung aus dem Temperaturfeld erstellt
#Die Rueckgabe ist der Vektor mit laenge (N+1)^2
def erstelle_Vektor(T,N=10):
	na  = N+1
	nn  = na*na
	x   = np.zeros(nn)
	for i in range(na):
		for j in range(na):
			x[i*na+j] = T[i][j]
	return x

#Hier werden die Startwerte in das Temperaturfeld geschrieben
def startwerte(N=10,Pe=0.7):
	T = np.zeros((N+1,N+1))
	for i in range(N+1):
		for j in range(N+1):
			T [i][j] = j/float(N)
	return T

#Hier wird der Vektor wieder in das Temperaturfeld umgeschrieben
def hole_aus_vektor(x,N=10):
	T   = np.zeros((N+1,N+1))
	for i in range(N+1):
            for j in range(N+1):
               T[i][j]	= x[i*(N+1)+j]
	return T

#Diese Funktion fuert die Berechnung durch und gibt entweder einen Plot oder die Rechenzeiten aus
#Die Parameter und der Zeitschritt sind so gewaehlt, dass das Ergebnis der Arbeit erzeugt wird
#Die Hier verwendeten Loesungsverfahren sind auch die, die mit diesem Beispiel ausgewerte wurden
#Die andere geben entweder Fehler aus, oder brauchen Stunden zum konvergieren
def beispiel(N=30,Pe=10.0,dt=100.0, vergleich=False):
	T	= startwerte(N=N,Pe=Pe)
	x	= erstelle_Vektor(T,N=N)
	A	= erstelle_Matrix(N=N,Pe=Pe,dt=dt)
	#Abfrage ob der Vergleich oder nur die Loesung ausgeben werden soll
	if not(vergleich):
		import numpy.linalg as nl
		start   = ti.time()
		y       = nl.solve(A,x)
		stop    = ti.time()
		print "NUMPY serial:\t", (stop-start), "sec"
		T	= hole_aus_vektor(y,N=N)
                #erstellen des plots
                #Programm beendet erst, wenn der Plot geschlossen wird und anders herum
		fig = plt.figure()
		plt.imshow(np.transpose(T), interpolation='none', aspect='equal', origin='lower')
		plt.colorbar()
		plt.show()
	else:
		#alle Notwendigen Imports
		import linearsolver.serial.lu as slu
		import linearsolver.serial.qr as sqr
		import linearsolver.opencl.lu as olu
		import linearsolver.serial.gs as sgs
		import linearsolver.opencl.gs as ogs
		import linearsolver.distributed.solver_petsc4py as skr
		import numpy.linalg as nl

		start_SLU   = ti.time()
		y           = slu.loesen(A,x)
		stop_SLU    = ti.time()
		print "LU serial:\t", (stop_SLU-start_SLU), "sec"
		y           = olu.loesen(A,x)
		stop_OLU    = ti.time()
		print "LU OpenCL:\t", (stop_OLU-stop_SLU), "sec"
		y           = sqr.loesen(A,x) #gibt eine Warnung aus, die nicht behoben ist
		stop_SQR    = ti.time()
		print "QR Seriell:\t", (stop_SQR-stop_OLU), "sec"
		y           = sgs.loesen(A,x)
		stop_SGS    = ti.time()
		print "GS serial:\t", (stop_SGS-stop_SQR), "sec"
		y           = skr.loesen(A,x)
		stop_SKR    = ti.time()
		print "PET serial:\t", (stop_SKR-stop_SGS), "sec"
		y           = nl.solve(A,x)
		stop_N      = ti.time()
		print "NUMPY serial:\t", (stop_N-stop_SKR), "sec"

#Sollte dieses als Script ausgefuehrt werden, wird das Beispiel geladen
if __name__ == "__main__":
    beispiel(N=30, vergleich=True)
