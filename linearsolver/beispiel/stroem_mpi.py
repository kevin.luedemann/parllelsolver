#!/usr/bin/python
"""
Beispielsprogramm fuer eine Anwendung des PETSc (MPI) Modul auf ein echtes Problem.
Hier wird eine zweidimensionale Diffusions-Advektions-Gleichung mit inversem zeitschritt geloest.
Das Programm erstellt die noetigen Matrizen und Vektoren.
Anschliessend werden diese mit dem Modul berechnet.
Zum ausfuehren muss mpirun verwendet werden
	mpirun -n 2 stroem_mpi.py	#als Beispiel

Author: Kevin Luedemann
EMail: kevin.luedemann@stud.uni-goettingen.de
"""

import numpy as np
import matplotlib as mp
#Abfrage ob ein Graphisches Backend gewaehlt wurde oder nicht
#Fals Nein wird es ausgewahlt
backend =  mp.get_backend()
if backend == 'agg':
	mp.use('TkAgg')
import matplotlib.pyplot as plt
import scipy as sp
from pylab import *
import time as ti

#Hier wird die Matrix aus dem Schema fuer den inversen Zeitschritt erstellt
#Es wird ein dense Schema verwendet
#Dies kann aber durch konvertierung geandert werden
def erstelle_Matrix(Pe=14, dt=100.0, N=10):
    dx  = 1.0/float(N+1)
    s   = dt*Pe/(2.0*dx)
    k   = dt/(dx*dx)
    nn  = (N+1)*(N+1)
    na  = N+1
    A   = np.zeros((nn,nn))
    x   = np.linspace(0,1,na)
    y   = np.linspace(0,1,na)

    #Geschwidigkeitsvektoren (siehe Hausarbeit)
    vx  = np.outer(sin(2.0*pi*x),cos(pi*y))*pi
    vy  = -1*np.outer(cos(2.0*pi*x),sin(pi*y))*2.0*pi

    #Iteration ueber die Matrixelemente
    #kann uber stencel optimiert werden
    #fuer Schema siehe Hausarbeit
    for i in range(na):
        for j in range(na):
            A[i*na+j][i*na+j] = 1.0*4.0*k
            if j==0 or j==N:
                A[i*na+j][i*na+j] = 1.0
            else:
                A[i*na+j][i*na+j-1] = -(s*vy[i][j]+k)
                A[i*na+j][i*na+j+1] = s*vy[i][j]-k
            if i==0 and j != 0 and j != N:
                A[i*na+j][(i+1)*na+j] = -2.0*k
            elif i==N and j != 0 and j != N:
                A[i*na+j][(i-1)*na+j] = -2.0*k
            elif j != 0 and j != N:
                A[i*na+j][(i-1)*na+j] = -(s*vx[i][j]+k)
                A[i*na+j][(i+1)*na+j] = (s*vx[i][j]-k)
    return A

#Mit dieser Funktion wird der Vektor fuer die Berechnung aus dem Temperaturfeld erstellt
#Die Rueckgabe ist der Vektor und hat die grosse (N+1)^2
def erstelle_Vektor(T,N=10):
	na  = N+1
	nn  = na*na
	x   = np.zeros(nn)
	for i in range(na):
		for j in range(na):
			x[i*na+j] = T[i][j]
	return x

#Hier werden die Startwerte in das Temperaturfeld geschrieben
def startwerte(N=10,Pe=0.7):
	T = np.zeros((N+1,N+1))
	for i in range(N+1):
		for j in range(N+1):
			T [i][j] = j/float(N)
	return T

#Hier wird der Vektor wieder in das Temperaturfeld umgeschrieben
#in dieser Funktion werden auch die Lokalen indizes Verwendet
def hole_aus_vektor(x,N=10):
	n   = N+1
	T   = np.zeros((N+1,N+1))
        #holen der lokalen Indizes
        #Funktioniert nur, wenn die loesen_mpi Funktion verwendet wird
        #es wird ein PETSc Vektorobjekt benoetigt
	n1,m1	= x.getOwnershipRange()
	for i in range(n1,m1):
		T[i//n][i%n]	= x[i]
	return T

#Diese Funktion fuert die Berechnung durch und gibt die Rechenzeit aus
#Die Parameter und der Zeitschritt sind so gewaehlt, dass das Ergebnis der Arbeit erzeugt wird
#Hier wird die zweite Loesemethode von PETSc verwendet, die das PETSc Vektorobjekt zurueckgibt
#Ausserdem wird das Temperaturfeld lokal vollstaendig erzeugt und nur an den lokalen indizes gefuellt
def beispiel(N=30,Pe=10.0,dt=100.0):
	import linearsolver.distributed.solver_petsc4py as dkr
	T	= startwerte(N=N,Pe=Pe)
	x	= erstelle_Vektor(T,N=N)
	A	= erstelle_Matrix(N=N,Pe=Pe,dt=dt)
	start   = ti.time()
	y       = dkr.loesen_mpi(A,x)
	stop    = ti.time()
	T       = hole_aus_vektor(y,N=N)
	print "PETSc: ", (stop-start), "sec"

#Sollte dieses als Script ausgefuehrt werden, wird das Beispiel geladen
if __name__ == "__main__":
    beispiel(N=30)
