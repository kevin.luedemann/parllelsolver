#!/usr/bin/python
"""
Modul zur implementation der Loesungsverfahren.
Nur diese Datei muss eingebunden, es koenne aber auch die anderen direkt eingebunden werden.
Nur die Funktion loesen() muss ausgefuehrt werden.
Eine Hilfe ist mit der Routine help_loesen() vorhanden.
Ein Beispiel zur Ausfuehhrung ist ebenfalls vorhanden.

Author: Kevin Luedemann
EMail: kevin.luedemann@stud.uni-goettingen.de
"""

import numpy as np
import numpy.linalg as nl
import scipy as sp
import scipy.linalg as sl
import matplotlib
from pylab import *
import matplotlib.pyplot as plt
import sys
import time as ti


#Hilfe Funktion zum Anzeigen der Bedienugnsmoeglichkeiten des Loesers
def help_loesen():
	print """
Das Programm loesen() loest die lineare Gleichung Ax=y mit verschiedenen Verfahren.\n
Hierbei werden verwendet:\n
1. LU: Eine LU Zerlegung mit Pivotisierung
2. QR: Eine QR Zerlgeung mit Pivotisierung
3. J: Das Jacobi bzw. Gesammtschrittverfahren mit Pivotisierung
4. GS: Das Gauss-Seidel bzw. Einzelschrittverfahren
5. PET: Loeser aus dem PETSc Paket
\n
Ein Praefix wird verwendet zur unterscheidung der Verfahren:\n
1. S: fuer alle seriellen Verfahren
2. O: fuer alle opencl Verfahren
Falls kein Praefix gewaehlt, wird das serielle gewaehlt.
Nur LU, QR, J und GS koennen ein Praefix habe.
\n
Der Programmaufruf sieht wie folgt aus:\n
x = loesen(A,y,loeser=\"SGS\")\n
Die ersten beiden Parameter sind die Matrix A und der Loesungsvektor y.
Der letzte Parameter waehlt das Loesungsverfahren aus den oben genannten.
"""

# Programm aus Vorlesung Wissenschaftliches Rechnen mit Python uebernommen
# Dies dient als Basistestroutine zum validieren der Verfahren 
# Mit dieser Routine werden die Matrix und die Vektoren erstellt,
# welche anschliessend mit den jeweiligen Loesungsverfahren und den jeweiligen parallelierungen evaluiert werden.
def randwertaufgabe(n):
	"""
	berechnet mit Finiten Differenzen die Loesung u von
	-u''=f in (0,1), u(0)=u(1)=0
	n =  Anzahl Stuetzstellen
	@author Gerd Rapin, Jochen Schulz
	"""
	# Erzeugen des Gitters
	x = linspace(0,1,n)
	x_i = x[1:n-1]
	# Aufstellen des lin. Gls.
	A = diag(2*ones(n-2),0)+diag(-1*ones(n-3),-1)+diag(-1*ones(n-3),1)
	F = (1./n)**2*exp(x_i) # rechte Seite fuer f=exp(x) 
	return (A,F)

#Funktion zur Auswahl der Loeser
#Hier sind alle Loeser implementiert, es funktionieren aber nur die im Beispiel verwendeten
#Die Programmaufrufsmaske ist aehlich zu den einzelnen Modulen
#es werden die Loeser nur dan Includiert, wenn sie benoetigt werden
def loesen(A,y,loeser="GS", toll=1e-4):
	n,m = np.shape(A)
	x = np.zeros(n)

	#pivotisierung nur falls noetig
	if loeser == "SLU" or loeser == "LU" or loeser == "OLU" or loeser == "QR"  or loeser == "OQR" or loeser == "J" or loeser == "OJ":
		import linearsolver.serial.lu as slu
		P = slu.pivotmatrix(A)
		A = np.dot(P,A)
		y = np.dot(P,y)

	#waehlen des Loesungsverfahrens
	if loeser == "OLU":
		import linearsolver.opencl.lu as olu
		x = olu.loesen(A,y)
	elif loeser == "OQR":
		import linearsolver.opencl.qr as oqr
		x = oqr.loesen(A,y)
	elif loeser == "OGS":
		import linearsolver.opencl.gs as ogs
		x = ogs.loesen(A,y,toll=toll)
	elif loeser == "OJ":
		import linearsolver.opencl.j as oj
		x = oj.loesen(A,y,toll=toll)
        #hier ist die verweigte auswahl zu erkennen, beide werden akzeptiert
	elif loeser == "SLU" or loeser == "LU":
		x = slu.loesen(A,y)
	elif loeser == "SQR" or loeser == "QR":
		import linearsolver.serial.qr as sqr
		x = sqr.loesen(A,y)
	elif loeser == "SGS" or loeser == "GS":
		import linearsolver.serial.gs as sgs
		x = sgs.loesen(A,y,toll=toll)
	elif loeser == "SJ" or loeser == "J":
		import linearsolver.serial.j as sj
		x = sj.loesen(A,y,toll=toll)
	elif loeser == "PET" or loeser =="SPET":
		import linearsolver.distributed.solver_petsc4py as spet
		x = spet.loesen(A,y)
	#Verfahren nicht implementiert
	else:
		assert False, "loeser: \"" + loeser + "\" nicht implementiert"
	return x

#Beispielprogramm zur Veranschaulichung der Nutzung des hier verwendeten Loeserpakets
#Es wird jeweils die Zeit, die zur Berechnung benoetigt wird ausgegeben
#Dies sind alle Verfahren, die funktionieren, abgesehen von PETSc mit MPI
def beispiel(N=10,toll=1e-4):
        #alle Loeser bekommen die gleiche Matrix und Loesungsvektor
	A,y = randwertaufgabe(N)

	print "iterativ:"
	start = ti.time()
	z_lu = loesen(A,y,loeser="SLU")
	stopluit = ti.time()
	print "LU:\t" + "{}".format(stopluit-start)
	z_gs	= loesen(A,y,loeser="SGS",toll=toll)
	stopgsit	= ti.time()
	print "GS:\t" + "{}".format(stopgsit-stopluit)
	z_j	= loesen(A,y,loeser="SJ",toll=toll)
	stopjit	= ti.time()
	print "J:\t" + "{}".format(stopjit-stopgsit)
	z_qr	= loesen(A,y,loeser="SQR")
	stopqrit	= ti.time()
	print "QR:\t" + "{}".format(stopqrit-stopjit)
	z_sp	= sl.solve(A,y)
	stopsp	= ti.time()
	print "SCIPY:\t" + "{}".format(stopsp-stopqrit)
	z_np	= nl.solve(A,y)
	stopnp	= ti.time()
	print "NUMPY:\t" + "{}".format(stopnp-stopsp)

	print "PyViennacl:"
	z_lu = loesen(A,y,loeser="OLU")
	stopluop = ti.time()
	print "LU:\t" + "{}".format(stopluop-stopnp)
	z_gs	= loesen(A,y,loeser="OGS",toll=toll)
	stopgsop	= ti.time()
	print "GS:\t" + "{}".format(stopgsop-stopluop)

	print "PETSc:"
	z_kr    = loesen(A,y,loeser="PET")
	stopskr = ti.time()
	print "PET:\t" + "{}".format(stopskr-stopgsop)


if __name__ == "__main__":
	help_loesen()
	beispiel(N=100,toll=1e-4)
