#!/usr/bin/python
"""
Skript fuer die LU Zerlegung in serieller Form
Die Loesung beruht allein auf Numpy Arrays und Pythonschleifen

Author: Kevin Luedemann
EMail: kevin.luedemann@stud.uni-goettingen.de
"""

import numpy as np

#diese beiden Funktionen sind auch implementierbar ueber dieses Modul
#Vorwaertssubstitution zum Loesen der ersten Gleichung
def vorwaerts(L,b):
	#Uebersetzen in Numpy Array, damit die Indexierung der Spalten leichter ist
	L=np.array(L)
	N1,N2 = np.shape(L)
	N3 = len(b)
	assert N2==N3, "Matrixdimension passt nicht mit Vektordimension ueberein"
	x = np.zeros(len(b))
	#vorwaertsalgorythmuss
	for ii in range(0,N1):
		x[ii]=b[ii]-np.dot(L[ii,:],x)
		x[ii]=x[ii]/L[ii,ii]
	return x

#Rueckwaertssubstitution zum Loesen der zweiten Gleichung
def rueckwaerts(U,b):
	#Uebersetzen in Numpy Array, damit die Indexierung der Spalten leichter ist
	U = np.array(U)
	N1,N2 = np.shape(U)
	N3 = len(b)
	assert N2==N3, "Matrixdimension passt nicht mit Vektordimension ueberein"
	x = np.zeros(N3)
	#rueckwaertsalgorythmus
	for ii in range(N1-1,-1,-1):
		x[ii] = b[ii]-np.dot(U[ii,:],x)
		x[ii] = x[ii]/U[ii,ii]
	return x

#Diese Funktion ist ebenfalls extern nuetzlich
#Erstellen der Pivotmatrix
#Vertauschen von Zeilen, so dass die Groessten Elemente auf der Hauptdiagonalen stehen
def pivotmatrix(A):
	A = np.array(A)
	n,m = np.shape(A)
	assert n==m, "Noch nicht behandelte Ausnahme"
	I = [[float(i==j) for i in range(m)] for j in range(m)]
	for ii in range(0,n):
                #Auswahlen der Indizes, die vertauscht werden sollen
                #keine Perfekte Pivotisierung, aber es bleiben keine Nullen auf der Hauptdiagonalen uebrig
		index = max(range(ii,n),key=lambda i: np.abs(A[i,ii]))
		if index != ii:
			temp = I[ii]
			I[ii] = I[index]
			I[index] = temp
	return I


#Erstellen der LU Zerlegung
#Rueckgabe sind L und U in zwei getrennten Arrays
def luzerlegung(A):
	A=np.array(A)
	n=len(A)
	A=np.dot(pivotmatrix(A),A)
	L = np.eye(n,n)
	U = A
	for i in range(n):
		for j in range(i+1,n):
			L[j,i] = U[j,i]/U[i,i]
			U[j,i:]= U[j,i:]-L[j,i]*U[i,i:]
			U[j,i] = 0
	return L,U

#Funktion zum Loesen des Gleichungssystems
def loesen(A,y):
	N1,N2 = np.shape(A)
	N3 = len(y)
	assert N2==N3, "Matrixdimension passt nicht mit Vektordimension ueberein"
	L,U = luzerlegung(A)
        #dieser Teil laesst sich optimieren in dem die jeweiligen Numpy Funktionen verwendet werden
	z = vorwaerts(L,y)
	x = rueckwaerts(U,z)
	return x

#Beispielprogramm zur Validierung
#Dieses Beispiel liesse sich auch per Hand rechnen
def beispiel():
	A = np.array([[4.0, -2.0, 1.0], [1.0, -3.0, 2.0], [-1.0, 2.0, 6.0]])
	y = [1.0, 2.0, 3.0]
	import scipy.linalg as sl
        #Vergleich zwischen diesem Loeser und dem Scipy Loeser
	print sl.solve(A,y)
	print loesen(A,y)

if __name__ == "__main__":
	beispiel()
