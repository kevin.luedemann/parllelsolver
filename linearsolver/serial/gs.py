#!/usr/bin/python
"""
Programm zur Darstellung des Gauss-Seidel-Verfahrens
Dieses Berechnet die Loesung eines linearen Gleichungssystems iterativ
Desweiteren ist die Toleranz mit in dem Loeser anzugeben

Author: Kevin Luedemann
EMail: kevin.luedemann@stud.uni-goettingen.de
"""

import numpy as np
import numpy.linalg as nl
import lu as slu

#Zerlegen der Matrix in die noetigen Stuecke
#hier muss optimiert werden wenn ein sparse System verwendet werden soll
def zerlege_Matrix_gausseidel(A):
	return np.triu(A,1),np.tril(A)

#Loesen des Gleichungssystem
#zusaetzlich kann die Toleranz angegeben werden
def loesen(A,y,toll=1e-7):
	n,m	= np.shape(A)
	n1	= len(y)
	assert n==n1, "Matrixdimension passt nicht mit Vektordimension ueberein"
	AU, ADL	= zerlege_Matrix_gausseidel(A)
	x	= np.ones(n)
        #erstellen des Residuums
	r	= np.dot(A,x)-y
	#iteration des Verfahrens
        #nur das Matrixprodukt wird aus Numpy verwendet
	while nl.norm(r) >= toll:
            x = slu.vorwaerts(ADL,(y-np.dot(AU,x)))
            r = np.dot(A,x)-y
	return x

#Funktion zum validieren der Iteration
#Ein Vergleich ist hier mit dem Jacobi-Verfahren Moeglich
def beispiel():
	A = np.array([[4.0, -2.0, 1.0], [1.0, -3.0, 2.0], [-1.0, 2.0, 6.0]])
	y = [1.0, 2.0, 3.0]
	print loesen(A,y)

if __name__ == "__main__":
	beispiel()
