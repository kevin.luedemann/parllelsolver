#!/usr/bin/python
"""
Programm zur Darstellung der QR Zerlegung nach Household
serielle Berechnung
Ebenfalls moeglich ist die Ausgabe der Zerlegten Matrizen

Author: Kevin Luedemann
EMail: kevin.luedemann@stud.uni-goettingen.de
"""

import numpy as np
import numpy.linalg as nl
import lu as slu

#Erzeugen der QR Zerlegung
#Rueckgabe sind die Matrizen Q und R als getrennte Arrays
def qr_zerlegung(A):
	A = np.array(A)
	n,m = np.shape(A)
	assert n==m, "Matrixdimension nicht quadratisch"

	I = np.eye(n,m)
	Q = np.eye(n,m)

	#zerlegung nach Housholder
	for k in range (n-1):
                #Erstellung der Vektoren fuer die Housholdermatrix
		x = np.transpose(np.copy(A[:,[k]]))[0]
		x[:k] = 0.0
		e = np.transpose(I[:,[k]])[0]

		alpha = -np.sign(x[k])*nl.norm(x)
		u = x+alpha*e
		v = np.transpose(u/nl.norm(u))

		#erzeuge neue Householder Matrix mithilfe des aeusseren Produktes
		Qk = np.copy(I)
		Qk -= 2.0*np.outer(v,v)

		Q = np.dot(Q,Qk)
		A = np.dot(Qk,A)
	return Q,A

#Funktion zum Loesen von Linearen Gleichungen
#hier wird die Zerlegungsfunktion von oben verwendet
def loesen(A,y):
	N1,N2 = np.shape(A)
	N3 = len(y)
	assert N2==N3, "Matrixdimension passt nicht mit Vektordimension ueberein"
	Q,R = qr_zerlegung(A)
        #Zuerst wird mit der Transponierten Matrix Multipliziert und anschliessend wird Rueckwartseingesetzt
	x = slu.rueckwaerts(R,np.dot(np.transpose(Q),y))
	return x

#Beispiel zur Validierung des Algorithmussen
#dieses kann auch per Hadn durchgerechnet werden
#hier wird mit der Zerlgeung von Scipy verglichen
def beispiel():
	import scipy.linalg as sl
	A = [[12, -51, 4], [6, 167, -68], [-4, 24, -41]]

	Q,R = qr_zerlegung(A)
	Q2,R2 = sl.qr(A)

	print "Q"
	print Q
	print Q2, "\n"
	print "R"
	print R
	print R2

if __name__ == "__main__":
	beispiel()
