#!/usr/bin/python
"""
Programm zur Darstellung des Jacobi Verfarens
Dieses loest ein gegebenes lineares Geleichungssystem
Weiterhin kann die Toleranz mit angegeben werden

Author: Kevin Luedemann
EMail: kevin.luedemann@stud.uni-goettingen.de
"""

import numpy as np
import numpy.linalg as nl

#Zerlegen der Matrix in die notwendigen Teile
#hier muss optimiert werden, um ein sparse System zu erlauben
def zerlege_Matrix_Jacobi(A):
	return np.triu(A,1),np.diag(A),np.tril(A,-1)

#Loesen eines gegebenen linearen Gleichungssystems
def loesen(A,y,toll=1e-4):
	n,m		= np.shape(A)
	n1		= len(y)
	assert n==n1, "Matrixdimension passt nicht mit Vektordimension ueberein"
	AU, AD_lin, AL	= zerlege_Matrix_Jacobi(A)
	AG		= AU+AL
	x		= np.ones(n)
	AD_inv		= 1.0/AD_lin
	I		= np.eye(n)
	AD		= I*AD_inv
	r               = np.dot(A,x)-y
	#iteration des Verfahrens
        #die Produkte werden mithilfe von Numpy ausgerechnet
	while nl.norm(r) >= toll:
		x   = -np.dot(AD,np.dot(AG,x)) + np.dot(AD,y)
		r   = np.dot(A,x)-y
	return x

#Beispiel Funktion zur Validierung des Verfahrens
#Ein Vergleich ist mit dem Gauss-Seidel-Verfahren moeglich
def beispiel():
	A = np.array([[4.0, -2.0, 1.0], [1.0, -3.0, 2.0], [-1.0, 2.0, 6.0]])
	y = [1.0, 2.0, 3.0]
	print loesen(A,y)

if __name__ == "__main__":
	beispiel()
