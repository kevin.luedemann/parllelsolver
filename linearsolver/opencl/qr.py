#!/usr/bin/python
"""
Programm zur Darstellung der QR Zerlegung nach Household
Hier werden die Produkte durch Pyviennacl berechnet
Diese Implementierung unterstuetzt nur 32Bit berechnungen

!!!!
Achtung
!!!!
Diese Verfahren gibt bei der Berechung einen Fehler aus, der noch nicht behoben ist

Author: Kevin Luedemann
EMail: kevin.luedemann@stud.uni-goettingen.de
"""

import numpy as np
import numpy.linalg as nl
import pyviennacl as p

#QR Zerlegung
#Rueckgabe der Zerlegten Matrix, falls zum Beispiel Eigenwerte bestimmt werden sollen
def qr_zerlegung(A):
	A = np.float32(np.array(A))
	n,m = np.shape(A)

	I = np.float32(np.eye(n,m))
	Q = np.float32(np.eye(n,m))

	#Reservieren und Kopieren der Array in den Graphikspeicher
	I_opencl    = p.Matrix(I)
	Qk_opencl   = p.Matrix(I)
	Q_opencl    = p.Matrix(Q)
	A_opencl    = p.Matrix(A)

	#Eigentliche Zerlegung
	for k in range (n-1):
                #Erstellen der Vektoren fuer die Housholdermatrizen
		x = np.transpose(np.copy(A[:,[k]]))[0]
		x[:k] = 0.0
		e = np.transpose(I[:,[k]])[0]

		alpha = -np.sign(x[k])*nl.norm(x)
		u = x+alpha*e
		v = np.transpose(u/nl.norm(u))
		v_opencl    = p.Vector(v)

		#diese Berechnung fuehrt zu den Fehlern
		#Der Fehler liegt aber in Pyviennacl
		Qk_opencl = v_opencl.outer(v_opencl) * np.float32(2.0)
		Qk_opencl = I_opencl - p.Matrix(Qk_opencl.value)

		Q_opencl = Q_opencl * Qk_opencl
		A_opencl = Qk_opencl * A_opencl
	return Q_opencl,A_opencl

#Loesen von linearen Gleichungen
#Hier wird die Zerlegung von oben auch wieder erzeugt, aber nicht zuruck gegeben
def loesen(A,y):
	N1,N2 = np.shape(A)
	N3 = len(y)
	assert N2==N3, "Matrixdimension passt nicht mit Vektordimension ueberein"
	A = np.float32(np.array(A))
	n,m = np.shape(A)

	I = np.float32(np.eye(n,m))
	Q = np.float32(np.eye(n,m))

	#reservieren und Kopieren der Arrays in den Graphikspeicher
	I_opencl    = p.Matrix(I)
	Qk_opencl   = p.Matrix(I)
	Q_opencl    = p.Matrix(Q)
	A_opencl    = p.Matrix(A)
	y_opencl    = p.Vector(np.float32(np.array(y)))

	#Eigentliche Zerlegung
	for k in range (n-1):
                #Erstellen der Vektoren fuer die Housholdermatrizen
		x = np.transpose(np.copy(A[:,[k]]))[0]
		x[:k] = 0.0
		e = np.transpose(I[:,[k]])[0]

		alpha = -np.sign(x[k])*nl.norm(x)
		u = x+alpha*e
		v = np.transpose(u/nl.norm(u))
		v_opencl    = p.Vector(v)

		#diese Berechnung fuehrt zu den Fehlern
		#Der Fehler liegt aber in Pyviennacl
		Qk_opencl = v_opencl.outer(v_opencl) * np.float32(2.0)
		Qk_opencl = I_opencl - p.Matrix(Qk_opencl.value)

		Q_opencl = Q_opencl * Qk_opencl
		A_opencl = Qk_opencl * A_opencl
		A = A_opencl.value

	#loesen des Gleichungssystems
        #auch hier wird das rueckwaertseinsetzten von pyviennacl uebernommen
	y_opencl = p.Trans(Q_opencl.execute()) * y_opencl
	y_opencl = p.solve(A_opencl.execute(),y_opencl.execute(),p.upper_tag())
	return y_opencl

#Beispiel zur Validierung der Zerlegung
#dieses Beispiel kann auch per hand durchgerechnet werden
#verglichen wird mit der Zerlegung von Scipy
#bei der Ausgabe muss auf die Schreibweise der Zahlen geachtet werden
def beispiel():
	import scipy.linalg as sl
	A = [[12, -51, 4], [6, 167, -68], [-4, 24, -41]]

	Q_op,R_op = qr_zerlegung(A)
	Q2,R2 = sl.qr(A)

	print "Q"
	print Q_op
	print Q2, '\n'
	print "R"
	print R_op
	print R2,'\n'

if __name__ == "__main__":
	beispiel()
