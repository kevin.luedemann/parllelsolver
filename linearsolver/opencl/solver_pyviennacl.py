#!/usr/bin/python
"""
Programm zur Implementierung des iterativen Loesers von Pyviennacl
Dieser funktioniert leider nicht
Ein Moeglicher Fehler ist die ueberganbe des Tags
Dieser scheint fuer keinen des eingebauten Loeser von Viennacl zu funktionieren

!!!
Achtung
!!!
Dieses Modul erst benutzen, wenn der Fehler behoben ist
Es ist auch in der Demo nicht eingebaut

Fehler ist:
ArgumentError: Python argument types in
    pyviennacl._viennacl.iterative_solve(matrix_row_float, vector_float, bicgstab_tag)
did not match C++ signature:

Author: Kevin Luedemann
EMail: kevin.luedemann@stud.uni-goettingen.de
"""

import numpy as np
import pyviennacl as p
import linearsolver.demo as de

#Loeser fuer ein gegebenes lineares Gleichungssystem
#hierbei wird der Loeser von pyviennacl verwendet
def loesen(A,y,toll=1e-4):
    n,m = np.shape(A)
    n1  = len(y)
    assert n==n1, "Matrixdimension passt nicht mit Vektordimension ueberein"

    #32Bit formatieren und in den Graphikspeicher kopieren
    A   = np.array(A,dtype=np.float32)
    A_O = p.Matrix(A)
    b   = np.array(y,dtype=np.float32)
    b_O = p.Vector(b)

    #Hier wird der Tag fuer den Loeser definiert
    tag = p.bicgstab_tag(tolerance=toll)

    #Hier wird der Loeser erstellt und der Tag eingesetzt
    #keiner der Tags fuer die implementierten Loeser funktioniert
    y   = p.solve(A_O,b_O,tag)
    return y

#Beispielprogramm zur Validierung des Verfahrens
#Als Beispiel wird die Problemstellung aus der Demo verwendet
def beispiel():
    A,y = de.randwertaufgabe(n=100)
    print loesen(A,y)

if __name__ == "__main__":
    beispiel()
