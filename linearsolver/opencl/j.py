#!/usr/bin/python
"""
Programm zur Darstellung des Jacobi Verfahrens
Dieses loest ein gegebenen lineares Gleichungssystem
Es kann zusaetzlich die Toleranz mit angegeben werden
Die maximale Toleranz liegt bei 1e-7 aufgrund der 32Bit Berechnung

!!!!
Achtung
!!!!
Diese Verfahren gibt bei der Berechung einen Fehler aus, der noch nicht behoben ist

Author: Kevin Luedemann
EMail: kevin.luedemann@stud.uni-goettingen.de
"""

import numpy as np
import pyviennacl as p

#Zerlegen der matrix in die notwendigen Teile
#hier sollte optimiert werden um sparse Matrizen verwenden zu koennen
#dies spart Zeit beim Kopieren
def zerlege_Matrix_Jacobi(A):
	return np.triu(A,1),np.diag(A),np.tril(A,-1)

#loesen eines gegebenen linearen Gleichungssystems
#hier kann die Toleranz mit angegeben werden
#die maximale Toleranz liegt bei 1e-7 aufgrund der 32Bit Berechnung
def loesen(A,y,toll=1e-4):
	n,m = np.shape(A)
	n1  = len(y)
	assert n==n1, "Matrixdimension passt nicht mit Vektordimension ueberein"

	AU, AD_lin, AL	= zerlege_Matrix_Jacobi(A)
	AG          = AU+AL
	x           = np.ones(n)
	AD_inv      = 1.0/AD_lin
	I           = np.eye(n)
	AD          = I*AD_inv

        #kopieren der Arrays in den Graphik speicher
        #auch der Loesungsvektor wird mitkopiert zum spaeteren Loesen
	A_opencl    = p.Matrix(np.float32(A))
	AD_opencl   = p.Matrix(np.float32(AD))
	AG_opencl   = p.Matrix(np.float32(AG))
	x_opencl    = p.Vector(np.float32(x))
	y_opencl    = p.Vector(np.float32(y))
	ADy         = np.dot(AD,y)
	ADy_opencl  = p.Vector(np.float32(ADy))
	r_opencl    = A_opencl*x_opencl-y_opencl
	r_opencl.execute()  #Ausfuehren der ersten Berechnung fuer Abgleich
	#Iteration des Verfahrens
	while p.linalg.norm(r_opencl,ord=1).value >= toll:
            #es werden immer 10 Iterationen durchgefuert und dann das Residuum berechnet
            #hier kann optimiert werden um moeglichst wenig hin und herkopieren zu muessen
            for i in range(10):
                x_opencl = ADy_opencl - ((AG_opencl * x_opencl)*AD_opencl)
            r_opencl = A_opencl*x_opencl-y_opencl
            r_opencl.execute()
        #hier sollte der Wert bereits vorliegen, weil es fuer das Abbruchkriterium benoetigt wird
        return x_opencl.value

#Funktion zum Validieren des Verfahrens
#Das Ergebnis kann mit dem des Gauss-Seidel-Verfahrens verglichen weden
def beispiel():
	A = np.array([[4.0, -2.0, 1.0], [1.0, -3.0, 2.0], [-1.0, 2.0, 6.0]])
	y = [1.0, 2.0, 3.0]
	print loesen(A,y)

if __name__ == "__main__":
	beispiel()
