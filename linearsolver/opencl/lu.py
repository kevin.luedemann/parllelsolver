#!/usr/bin/python
"""
Skript fuer die LU Zerlegung in Pyviennacl Form
Hier werden die Produkte aus Pyviennacl
sowie die Vorwaerts und Rueckwaerts substitutionen verwendet

Author: Kevin Luedemann
EMail: kevin.luedemann@stud.uni-goettingen.de
"""

import numpy as np
import pyviennacl as p
import linearsolver.serial.lu as slu

#Funktion zum loesen von Gleichungssystemen
#Hier existiert keine Funktion, die die Zerlegung zurueck gibt
def loesen(A,b):
    N1,N2 = np.shape(A)
    N3 = len(b)
    assert N2==N3, "Matrixdimension passt nicht mit Vektordimension ueberein"
    #umwandeln, damit kein Problem bei berechnung auf 32bit GPU
    A = np.array(A)
    b = np.float32(b)
    n = len(A)
    A = np.float32(np.dot(slu.pivotmatrix(A),A))
    L = np.float32(np.eye(n,n))

    #kopieren in den Graphik speicher
    A_opencl = p.Matrix(A)
    L_opencl = p.Matrix(L)
    b_opencl = p.Vector(b)

    #LU Algorithmus
    for i in range(n-1):
        for j in range(i+1,n):
            L_opencl[j,i]   = A_opencl[j,i].value/A_opencl[i,i]
            A_opencl[j,i+1:]  = A_opencl[j,i+1:]-A_opencl[i,i+1:]*L_opencl[j,i]

    #Loesen Vorwaerts und Rueckwaerts mithilfe von pyviennacl
    b_opencl = p.solve(L_opencl,b_opencl,p.unit_lower_tag())
    b_opencl = p.solve(A_opencl,b_opencl,p.upper_tag())
    #rueckgabe nur des Werts und nicht des Objekts
    #hier wird die eigentliche Berechnung erst gestartet
    return b_opencl.value

#Beispiel zur Validierung
#hier wird mit dem Loeser durch Scipy verglichen
def beispiel():
    import scipy.linalg as sl
    # Testmatrix
    A = [[-1,1,-2],[2,0,4],[1,5,6]]
    P = slu.pivotmatrix(A)
    y = [1,2,3]
    print "Berechnung richtig mit y=", y, "unter Beachtung des Pivot"
    print "LU:"
    print loesen(A,y)
    print "scipy.linalg.solve"
    print sl.solve(np.dot(P,A),y)

if __name__ == "__main__":
	beispiel()
