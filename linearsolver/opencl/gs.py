#!/usr/bin/python
"""
Programm zur Darstellung des Gauss-Seidel-Verfahrens
Dieses Berechnet die Loesung eines linearen Gleichungssystems iterativ und per OpenCL
Es kann hier die Toleranz mit angegeben werden
Die maximal zulaessige Toleranz liegt bei 1e-7 aufgrund der 32Bit restriktion

Author: Kevin Luedemann
EMail: kevin.luedemann@stud.uni-goettingen.de
"""

import numpy as np
import pyviennacl as p

#Zerlegen der Matrix in die noetigen Teile
#hier muss optimiert werden um ein sparse Sytem zu erlauben
#die wurde Zeit beim Kopieren sparen
def zerlege_Matrix_gausseidel(A):
	return np.triu(A,1),np.tril(A)

#Loesen des Linearen Gleichungssystems
#Hier kann zusaetzlich die Toleranz angegeben werden
#maximale Toleranz ist 1e-7
def loesen(A,y,toll=1e-4):
	n,m = np.shape(A)
	n1  = len(y)
	assert n==n1, "Matrixdimension passt nicht mit Vektordimension ueberein"
	assert toll>=1e-7, "Toleranz zu klein fuer 32Bit Berechnungen"
	AU, ADL = zerlege_Matrix_gausseidel(A)
	x	= np.ones(n)

	#Kopiieren der Array in den Graphikspeicher
	A_opencl    = p.Matrix(np.float32(A))
	AU_opencl   = p.Matrix(np.float32(AU))
	ADL_opencl  = p.Matrix(np.float32(ADL))
	x_opencl    = p.Vector(np.float32(x))
	y_opencl    = p.Vector(np.float32(y))
	r_opencl    = A_opencl*x_opencl-y_opencl
	#Iteration des Verfahrens
	while p.linalg.norm(r_opencl,ord=1).value >= toll:
            #es werden immer 10 Iterationen durchgefuert und dann das Residuum berechnet
            #hier kann optimiert werden um moeglichst wenig hin und herkopieren zu muessen
            for i in range(10):
                x_opencl = y_opencl - (AU_opencl*x_opencl)
                x_opencl = p.linalg.solve(ADL_opencl,p.Vector(x_opencl), p.lower_tag())
            r_opencl = A_opencl*x_opencl-y_opencl
        #der Vektor sollte bereits berechnet sein, da er fuer die Abbruchbedingung benoetigt wird
	return x_opencl.value

#Funktion zum Vailidieren des Verfahrens
#Ein Vergleich ist mit dem Jacobi Verfahren moeglich
def beispiel():
	A = np.array([[4.0, -2.0, 1.0], [1.0, -3.0, 2.0], [-1.0, 2.0, 6.0]])
	y = [1.0, 2.0, 3.0]
	print loesen(A,y)

if __name__ == "__main__":
	beispiel()
