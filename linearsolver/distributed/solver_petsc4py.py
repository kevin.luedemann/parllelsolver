#!/usr/bin/python
"""
Programm zur Implementierung des PETSc4py Pakets und damit den PETSc Loeser
Hier werden zwei Arten der Implementierung gegeben
Die erste ist eine "serielle" (auch parallel verwendbar)
    und auch ueber "loesen" aufrufbar und gibt ein Numpy Array zureuck
Die zweite ist die mithilfe von MPI parallelisierte Version
Diese ist uber "loesen_mpi" aufrufbar und gibt des PETSc Vektor Objekt zurueck
Fuer das zweite muss das Script mit
	mpirun -n Anzahl_Prozesse/Kerne python Scriptname.py
aufgerufen werden
Beispiel:
	mpirun -n 2 python solver_petsc4py.py

Author: Kevin Luedemann
EMail: kevin.luedemann@stud.uni-goettingen.de
"""

#import des system und PETSc pakets
import sys, petsc4py
#Dieser Aufruf muss am Anfang von jedem PETSc4py Scripts stehen
#dies initalisiert das Paket und auch die MPI Anbindung
petsc4py.init(sys.argv)

from petsc4py import PETSc
import numpy as np
import time as ti
import linearsolver.demo as de

#Implementierung der seriellen Variante des Loesers
#Diese kann auch parallel verwendet werden
#allerdings wird das Ergebnis als Numpy Array zurueckgegeben
#dies kann nur schlecht parallel weiter verarbeitet werden
def loesen(AA,bb):
	n,m	= AA.shape
	n1	= len(bb)
	assert n==n1, "Matrixdimension passt nicht mit Vektordimension ueberein"
	#erstellen des Matrixobjektes
	#hier wird der globale Kommunikator uebergeben
	#dieser wird intern gehaendelt
	#er gibt an welche Indizes von welchem Prozess verarbeitet werden sollen
	A	= PETSc.Mat().create(PETSc.COMM_WORLD)
	#lege die groesse fest und, dass die Eigenschaften der Matrix aus moeglichen Uebergabeparameter 
	A.setSizes([n,m])
	A.setFromOptions()
	A.setUp()
	#hole die lokalen Indizes fuer A
	n1,m1	= A.getOwnershipRange()
	#iteriere die Matrix ueber alle lokalen indizes
	for i in range(n1,m1):
		for j in range (n1,m1):
			temp = AA[i,j]
			#kopiere nur Werte, die ungleich 0 sind (sparse Format)
			if temp != 0.0:
				A[i,j] = AA[i,j]
	#erstellen der Matrix
	#setze sie global zusammen
	A.assemblyBegin()
	A.assemblyEnd()
	#erstelle die Vektoren anhand der Form der Matrix
	x,b	= A.createVecs()
	#setze den Startvektor vollstaendig auf 1.0
	x.set(1.0)

	#hole die lokalen indizes fuer b und kopiere die Werte in den Loesungsvektor
	n1,m1	= b.getOwnershipRange()
	for i in range(n1,m1):
		b[i] = bb[i]
	#erstellen des Vektors
	#setzt den Vektor global zusammen
	b.assemblyBegin()
	b.assemblyEnd()

	#erstellen des Loeser objektes
	#in diesem Falls ein Krylov subspace loeser
	ksp = PETSc.KSP().create()
	#uebergebe den Operator (Matrix)
	ksp.setOperators(A)
	#stelle das Loesungsverfahren ein
	#dieses ist das einzige, das richtige Ergebnisse fuer das Beispielprogramm liefert
	#falls dies fuer eine andere Anwendungs nicht der Fall ist,
	#muss aus der Dokumentation von PETSc (C bzw. Fortran Modul) ein anderer Verwendet werden (Link in der Hausarbeit)
	ksp.setType('bcgs')
	#erzeuge einen Preconditioner und setzten ihn auf none (keinen)
	pc = ksp.getPC()
	pc.setType('none')
	#erstelle den Loeser mit den Eigenschaften, welche aus den Uebergabeparametern erhalten wurden
	ksp.setFromOptions()
	#loese das System
	ksp.solve(b, x)
	#uebergebe den Inhalt des PETSc Vektorobjektes als Numpy Array
	return x.getArray()

def loesen_mpi(AA,bb):
	n,m	= AA.shape
	n1	= len(bb)
	assert n==n1, "Matrixdimension passt nicht mit Vektordimension ueberein"
	#erstellen des Matrixobjektes
	#hier wird der globale Kommunikator uebergeben
	#dieser wird intern gehaendelt
	#er gibt an welche Indizes von welchem Prozess verarbeitet werden sollen
	A	= PETSc.Mat().create(PETSc.COMM_WORLD)
	#lege die groesse fest und, dass die Eigenschaften der Matrix aus moeglichen Uebergabeparameter gegeben sind
	A.setSizes([n,m])
	A.setFromOptions()
	A.setUp()
	#hole die lokalen Indizes fuer A
	n1,m1	= A.getOwnershipRange()
	#iteriere die Matrix ueber alle lokalen indizes
	for i in range(n1,m1):
		for j in range (n1,m1):
			temp = AA[i,j]
			#kopiere nur Werte, die ungleich 0 sind (sparse Format)
			if temp != 0.0:
				A[i,j] = AA[i,j]
	#erstellen der Matrix
	#setze sie global zusammen
	A.assemblyBegin()
	A.assemblyEnd()
	#erstelle die Vektoren anhand der Form der Matrix
	x,b	= A.createVecs()
	#setze den Startvektor vollstaendig auf 1.0
	x.set(1.0)

	#hole die lokalen indizes fuer b und kopiere die Werte in den Loesungsvektor
	n1,m1	= b.getOwnershipRange()
	for i in range(n1,m1):
		b[i] = bb[i]
	#erstellen des Vektors
	#setzt den Vektor global zusammen
	b.assemblyBegin()
	b.assemblyEnd()

	#erstellen des Loeser objektes
	#in diesem Falls ein Krylov subspace loeser
	ksp = PETSc.KSP().create()
	#uebergebe den Operator (Matrix)
	ksp.setOperators(A)
	#stelle das Loesungsverfahren ein
	#dieses ist das einzige, das richtige Ergebnisse fuer das Beispielprogramm liefert
	#falls dies fuer eine andere Anwendungs nicht der Fall ist,
	#muss aus der Dokumentation von PETSc (C bzw. Fortran Modul) ein anderer Verwendet werden (Link in der Hausarbeit)
	ksp.setType('bcgs')
	#erzeuge einen Preconditioner und setzten ihn auf none (keinen)
	pc = ksp.getPC()
	pc.setType('none')
	#erstelle den Loeser mit den Eigenschaften, welche aus den Uebergabeparametern erhalten wurden
	ksp.setFromOptions()
	#loese das System
	ksp.solve(b, x)
	#Ubergabe des PETSc Vektorobjektes
	#dier ermoeglicht die weiter Verarbeitung
	#es koennen zum beispiel von diesem die lokalen Indizes ausgelesen werden
	#dies ist die eleganteste Weise mit den Objekten weiter zu arbeiten
	return x

#Beispielprogramm zur Validierung der beiden Funktionen
#die erste Ausgabe ist der inhalt Des Loesungsvektors
#die zweite Ausgabe ist das PETSc Vektorobjekt aus der loesen_mpi Funktion
#ausserdem werden bei der zweiten ausgabe jeweils die lokalen Indizes mit ausgegeben
#dieses Programm ist parallel ausfuerhbar und zeigt das Problem beim parallelen ausgeben von Numpy Arrays
def beispiel():
    A,b		= de.randwertaufgabe(300)
    x		= loesen(A,b)
    print x
    x		= loesen_mpi(A,b)
    print x, "\tlokale Indizes: " ,x.getOwnershipRange()

if __name__ == "__main__":
    beispiel()
