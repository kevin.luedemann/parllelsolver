"""
Skript fuer die LU Zerlegung in terativer Form

Author: Kevin Luedemann
EMail: kevin.luedemann@stud.uni-goettingen.de

#internetQuelle= https://www.quantstart.com/articles/LU-Decomposition-in-Python-and-NumPy
"""

#Code aus internetquelle

import numpy as np
import scipy.linalg as sl

def vorwaerts(L,b):
	#uebersetzen in Numpy array, damit die indexierung der Spalten leichter ist
	L=np.array(L)
	N1,N2 = np.shape(L) 
	N3 = len(b)
	assert N2==N3, "Matrixdimension passt nicht mit Vektordimension ueberein"
	#Speicherplatz reservieren
	x = np.zeros(len(b))
	#vorwaertsalgorythmuss
	for ii in range(0,N1):
		x[ii]=b[ii]-np.dot(L[ii,:],x)
		x[ii]=x[ii]/L[ii,ii]
		
	return x

def rueckwaerts(U,b):
	#uebersetzen in Numpy array, damit die indexierung der Spalten leichter ist
	U = np.array(U)
	N1,N2 = np.shape(U) 
	N3 = len(b)
	assert N2==N3, "Matrixdimension passt nicht mit Vektordimension ueberein"
	#Speicherplatz reservieren
	x = np.zeros(N3)
	#rueckwaertsalgorythmus
	for ii in range(N1-1,-1,-1):
		x[ii] = b[ii]-np.dot(U[ii,:],x)
		x[ii] = x[ii]/U[ii,ii]
	return x

def pivotmatrix(A):
	A = np.array(A)
	n,m = np.shape(A)
	assert n==m, "Noch nicht behandelte Ausnahme"
	#I = np.eye(n,m)
	I = [[float(i==j) for i in range(m)] for j in range(m)]
	for ii in range(0,n):
		index = max(range(ii,n),key=lambda i: np.abs(A[i,ii]))
		if index != ii:
			#print I[ii],I[index]
			temp = I[ii]
			I[ii] = I[index]
			I[index] = temp
	return I


def luzerlegung(A):

	A=np.array(A)
	n=len(A)
	A=np.dot(pivotmatrix(A),A)
	L = np.eye(n,n)
	U = A
	for i in range(n):
		for j in range(i+1,n):
			L[j,i] = U[j,i]/U[i,i]
			U[j,i:]= U[j,i:]-L[j,i]*U[i,i:]
			U[j,i] = 0
	return L,U

def loesung_mit_lu(A,y):
	L,U = luzerlegung(A)
	z = vorwaerts(L,y)
	x = rueckwaerts(U,z)
	return x

if __name__ == "__main__":
	# Testmatrix
	#A = [[-1,1,-2],[2,4,0],[1,5,6]]
	#print np.array(A)
	#print np.dot(pivotmatrix(A),A)
	#P = pivotmatrix(A)
	#print P
	#L,U = luzerlegung(A)
	#print np.dot(P,L)
	#print U
	#P2,L2,U2 = sl.lu(A)
	#print "Ausgangsmatrix"
	#print np.array(A)
	#print "Pivotisierung richtig?"
	#print P==np.transpose(P2)
	#print "L richtig?"
	#print L==L2
	#print "U richtig?"
	#print U==U2
	#y = [1,2,3]
	#print "Berechnung richtig mit y=", y, "unter Beachtung des Pivot"
	#print "LU:"
	#print loesung_mit_lu(A,y)
	#print "scipy.linalg.solve"
	#print sl.solve(np.dot(P,A),y)
	#print "x=A^-1*y"
	#print np.dot(sl.inv(np.dot(P,A)),y)
	A = np.array([[4.0, -2.0, 1.0], [1.0, -3.0, 2.0], [-1.0, 2.0, 6.0]])
	y = [1.0, 2.0, 3.0]
	print sl.solve(A,y)
	print loesung_mit_lu(A,y)
