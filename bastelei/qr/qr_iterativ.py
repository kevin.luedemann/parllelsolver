"""
Programm zur Darstellung der QR Zerlegung nach Household

Author: Kevin Luedemann
EMail: kevin.luedemann@stud.uni-goettingen.de

Internetquelle = https://www.quantstart.com/articles/QR-Decomposition-with-Python-and-NumPy
"""

import numpy as np
import numpy.linalg as nl
import scipy.linalg as sl

def rueckwaerts(U,b):
   #uebersetzen in Numpy array, damit die indexierung der Spalten leichter ist
   U = np.array(U)
   N1,N2 = np.shape(U)
   N3 = len(b)
   assert N2==N3, "Matrixdimension passt nicht mit Vektordimension ueberein"
   #Speicherplatz reservieren
   x = np.zeros(N3)
   #rueckwaertsalgorythmus
   for ii in range(N1-1,-1,-1):
      x[ii] = b[ii]-np.dot(U[ii,:],x)
      x[ii] = x[ii]/U[ii,ii]
   return x

#aus Internetquelle uebernommene Funktion zur erzeugung von Q
#verbesserungswuerdig
def fuellenQ(Q,i,j,k):
	if i<k or j<k:
		return float(i==j)
	else:
		return Q[i-k][j-k]
"""
		#####################################
		x = (np.transpose(R)[k])[k:]
		e = (np.transpose(I)[k])[k:]
		alpha = -np.sign(x[0])*nl.norm(x)		
		
		u = map(lambda a,b: a+alpha*b, x, e)
		
		v = map(lambda x: x/nl.norm(u),u)
		Qh = [[float(i==j) - 2.0*v[i]*v[j] for i in range(n-k)] for j in range(m-k)]
		Qk = [[fuellenQ(Qh,i,j,k) for i in range(n)] for j in range(m)]
		#####################################
"""

def qr_zerlegung(A):
	A = np.array(A)
	n,m = np.shape(A)

	#assert n==m, "noch nicht behandelte Ausnahme"

	I = np.eye(n,m)
	Q = np.eye(n,m)
	
	for k in range (n-1): #Algorythmus bricht bei k=n-1 zusammen, da dann letzte Zeile nur ein Element beihnhaltet, das Projeziert 0 ist
		x = np.transpose(np.copy(A[:,[k]]))[0]
		x[:k] = 0.0
		e = np.transpose(I[:,[k]])[0]
		
		alpha = -np.sign(x[k])*nl.norm(x)		
		u = x+alpha*e
		v = np.transpose(u/nl.norm(u))
		
		#erzeuge neue Householder Matrix
		Qk = np.copy(I)
		Qk -= 2.0*np.outer(v,v)

		Q = np.dot(Q,Qk)
		A = np.dot(Qk,A)
		"""
		print "Q:", Q
		print "A:", A
		"""
	return np.transpose(Q),A

def loesen_mit_qr(A,y):
	Q,R = qr_zerlegung(A)
	x = sl.solve_triangular(R,np.dot(Q,y))
#	x = rueckwaerts(R,np.dot(Q,y))
	return x

if __name__ == "__main__":
	#Testmatix aus Internetquelle bzw. Wikipediaartikel
	A = [[12, -51, 4], [6, 167, -68], [-4, 24, -41]]

	Q,R = qr_zerlegung(A)
	Q2,R2 = sl.qr(A)
	
	print "Q"
	print Q
	print Q2, "\n"
	print "R"
	print R
	print R2










#code aus internetquelle
from math import sqrt
from pprint import pprint

def mult_matrix(M, N):
    """Multiply square matrices of same dimension M and N"""
    # Converts N into a list of tuples of columns                                                                     
    tuple_N = zip(*N)

    # Nested list comprehension to calculate matrix multiplication                                                    
    return [[sum(el_m * el_n for el_m, el_n in zip(row_m, col_n)) for col_n in tuple_N] for row_m in M]

def trans_matrix(M):
    """Take the transpose of a matrix."""
    n = len(M)
    return [[ M[i][j] for i in range(n)] for j in range(n)]

def norm(x):
    """Return the Euclidean norm of the vector x."""
    return sqrt(sum([x_i**2 for x_i in x]))

def Q_i(Q_min, i, j, k):
    """Construct the Q_t matrix by left-top padding the matrix Q                                                      
    with elements from the identity matrix."""
    if i < k or j < k:
        return float(i == j)
    else:
        return Q_min[i-k][j-k]

def householder(A):
    """Performs a Householder Reflections based QR Decomposition of the                                               
    matrix A. The function returns Q, an orthogonal matrix and R, an                                                  
    upper triangular matrix such that A = QR."""
    n = len(A)

    # Set R equal to A, and create Q as a zero matrix of the same size
    R = A
    Q = [[0.0] * n for i in xrange(n)]

    # The Householder procedure
    for k in range(n-1):  # We don't perform the procedure on a 1x1 matrix, so we reduce the index by 1
        # Create identity matrix of same size as A                                                                    
        I = [[float(i == j) for i in xrange(n)] for j in xrange(n)]

        # Create the vectors x, e and the scalar alpha
        # Python does not have a sgn function, so we use cmp instead
        x = [row[k] for row in R[k:]]
        e = [row[k] for row in I[k:]]
        alpha = -cmp(x[0],0) * norm(x)

        # Using anonymous functions, we create u and v
        u = map(lambda p,q: p + alpha * q, x, e)
        norm_u = norm(u) 
        v = map(lambda p: p/norm_u, u)
	print norm_u

        # Create the Q minor matrix
        Q_min = [ [float(i==j) - 2.0 * v[i] * v[j] for i in xrange(n-k)] for j in xrange(n-k) ]

        # "Pad out" the Q minor matrix with elements from the identity
        Q_t = [[ Q_i(Q_min,i,j,k) for i in xrange(n)] for j in xrange(n)]

        # If this is the first run through, right multiply by A,
        # else right multiply by Q
        if k == 0:
            Q = Q_t
            R = mult_matrix(Q_t,A)
        else:
            Q = mult_matrix(Q_t,Q)
            R = mult_matrix(Q_t,R)

    # Since Q is defined as the product of transposes of Q_t,
    # we need to take the transpose upon returning it
    return trans_matrix(Q), R

"""
A = [[12, -51, 4], [6, 167, -68], [-4, 24, -41]]
Q, R = householder(A)

print "A:"
pprint(A)

print "Q:"
pprint(Q)

print "R:"
pprint(R)
"""
