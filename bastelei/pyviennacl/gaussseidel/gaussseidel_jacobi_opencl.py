"""
Programm zur Darstellung des Gauss Seidel und Jacoby verrahrens

Author: Kevin Luedemann
EMail: kevin.luedemann@stud.uni-goettingen.de

Internetquelle = https://www.quantstart.com/articles/QR-Decomposition-with-Python-and-NumPy
"""

import numpy as np
import numpy.linalg as nl
import scipy.linalg as sl
import pyviennacl as p

def vorwaerts(L,b):
   #uebersetzen in Numpy array, damit die indexierung der Spalten leichter ist
   L=np.array(L)
   N1,N2 = np.shape(L)
   N3 = len(b)
   assert N2==N3, "Matrixdimension passt nicht mit Vektordimension ueberein"
   #Speicherplatz reservieren
   x = np.zeros(len(b))
   #vorwaertsalgorythmuss
   for ii in range(0,N1):
      x[ii]=b[ii]-np.dot(L[ii,:],x)
      x[ii]=x[ii]/L[ii,ii]

   return x


def zerlege_Matrix_Jacobi(A):
	return np.triu(A,1),np.diag(A),np.tril(A,-1)

def zerlege_Matrix_gausseidel(A):
	return np.triu(A,1),np.tril(A)

def jacobi_gesammtschritt(A,y,toll=1e-4):
	n,m			= np.shape(A)
	assert n==m, "Fehler in Dimensione: nicht quadratisch"
	AU, AD_lin, AL	= zerlege_Matrix_Jacobi(A)
	AG				= AU+AL
	x				= np.ones(n)
	AD_inv		= 1.0/AD_lin
	I				= np.eye(n)
	AD				= I*AD_inv
	r				= np.dot(A,x)-y
	while nl.norm(r) >= toll:
		x 		= -np.dot(AD,np.dot(AG,x)) + np.dot(AD,y)
		r		= np.dot(A,x)-y
	return x

def jacobi_gesammtschritt_opencl(A,y,toll=1e-4):
	n,m			= np.shape(A)
	assert n==m, "Fehler in Dimensione: nicht quadratisch"
	AU, AD_lin, AL	= zerlege_Matrix_Jacobi(A)
	AG		= AU+AL
	x		= np.ones(n)
	AD_inv		= 1.0/AD_lin
	I		= np.eye(n)
	AD		= I*AD_inv
	A_opencl			 = p.Matrix(np.float32(A))
        AD_opencl       = p.Matrix(np.float32(AD))
        AG_opencl       = p.Matrix(np.float32(AG))
        x_opencl        = p.Vector(np.float32(x))
        y_opencl        = p.Vector(np.float32(y))
        ADy             = np.dot(AD,y)
        ADy_opencl      = p.Vector(np.float32(ADy))
	r_opencl	= A_opencl*x_opencl-y_opencl
	while p.linalg.norm(r_opencl,ord=1).value >= toll:
	    x_opencl    = ADy_opencl - (AD_opencl * (AG_opencl * x_opencl))
            #x_opencl.execute()
	    r_opencl	= A_opencl*x_opencl-y_opencl
            #r_opencl.execute()
	return x_opencl

def gaussseidel_einzelschritt_opencl(A,y,toll=1e-4):
	n,m		= np.shape(A)
	assert n==m, "Fehler in Dimensione: nicht quadratisch"
	AU, ADL	= zerlege_Matrix_gausseidel(A)
	x	    = np.ones(n)

	A_opencl    = p.Matrix(np.float32(A))
        AU_opencl   = p.Matrix(np.float32(AU))
        ADL_opencl  = p.Matrix(np.float32(ADL))
        x_opencl    = p.Vector(np.float32(x))
        y_opencl    = p.Vector(np.float32(y))
	r_opencl    = A_opencl*x_opencl-y_opencl
	while p.linalg.norm(r_opencl,ord=1).value >= toll:
   	    x_opencl    = y_opencl - (AU_opencl*x_opencl)
            x_opencl    = p.linalg.solve(ADL_opencl,p.Vector(x_opencl), p.lower_tag())
	    r_opencl	= A_opencl*x_opencl-y_opencl
            #r_opencl.execute()
	return x_opencl

def gaussseidel_einzelschritt(A,y,toll=1e-4):
	n,m		= np.shape(A)
	assert n==m, "Fehler in Dimensione: nicht quadratisch"
	AU, ADL	= zerlege_Matrix_gausseidel(A)
	x	= np.ones(n)
	r	= np.dot(A,x)-y
	while nl.norm(r) >= toll:
		x = vorwaerts(ADL,(y-np.dot(AU,x)))
		r = np.dot(A,x)-y
	return x

if __name__ == "__main__":
	#A = [[12, -51, 4], [6, 167, -68], [-4, 24, -41]]
	#A = [[-1,1,-2],[2,4,0],[1,5,6]]
	#y = [1,2,3]
	A = np.array([[4.0, -2.0, 1.0], [1.0, -3.0, 2.0], [-1.0, 2.0, 6.0]])
	y = [1.0, 2.0, 3.0]
	print gaussseidel_einzelschritt(A,y)
	print jacobi_gesammtschritt(A,y)
	print "\n"
	print gaussseidel_einzelschritt_opencl(A,y)
	print jacobi_gesammtschritt_opencl(A,y)
