"""
Programm zur Darstellung der QR Zerlegung nach Household

Author: Kevin Luedemann
EMail: kevin.luedemann@stud.uni-goettingen.de

Internetquelle = https://www.quantstart.com/articles/QR-Decomposition-with-Python-and-NumPy
"""
#import pyviennacl as p
import numpy as np
import numpy.linalg as nl
import scipy.linalg as sl
import pyviennacl as p

def rueckwaerts(U,b):
   #uebersetzen in Numpy array, damit die indexierung der Spalten leichter ist
   U = np.array(U)
   N1,N2 = np.shape(U)
   N3 = len(b)
   assert N2==N3, "Matrixdimension passt nicht mit Vektordimension ueberein"
   #Speicherplatz reservieren
   x = np.zeros(N3)
   #rueckwaertsalgorythmus
   for ii in range(N1-1,-1,-1):
      x[ii] = b[ii]-np.dot(U[ii,:],x)
      x[ii] = x[ii]/U[ii,ii]
   return x

def qr_zerlegung(A):
	A = np.float32(np.array(A))
	n,m = np.shape(A)

	I = np.float32(np.eye(n,m))
	Q = np.float32(np.eye(n,m))
	
	for k in range (n-1): #Algorythmus bricht bei k=n-1 zusammen, da dann letzte Zeile nur ein Element beihnhaltet, das Projeziert 0 ist
		x = np.transpose(np.copy(A[:,[k]]))[0]
		x[:k] = 0.0
		e = np.transpose(I[:,[k]])[0]
		
		alpha = -np.sign(x[k])*nl.norm(x)		
		u = x+alpha*e
		v = np.transpose(u/nl.norm(u))
		
		#erzeuge neue Householder Matrix
		Qk = np.copy(I)
		Qk -= 2.0*np.outer(v,np.transpose(v))
	
		Q = np.dot(Q,Qk)
		A = np.dot(Qk,A)
		"""
		print "Q:", Q
		print "A:", A
		"""
	return Q,A

def qr_zerlegung_opencl(A):
	A = np.float32(np.array(A))
	n,m = np.shape(A)

	I = np.float32(np.eye(n,m))
	Q = np.float32(np.eye(n,m))
	
        I_opencl    = p.Matrix(I)
        Qk_opencl   = p.Matrix(I)
        Q_opencl    = p.Matrix(Q)
        A_opencl    = p.Matrix(A)

	for k in range (n-1): #Algorythmus bricht bei k=n-1 zusammen, da dann letzte Zeile nur ein Element beihnhaltet, das Projeziert 0 ist
		x = np.transpose(np.copy(A[:,[k]]))[0]
		x[:k] = 0.0
		e = np.transpose(I[:,[k]])[0]
		
		alpha = -np.sign(x[k])*nl.norm(x)		
                u = x+alpha*e	
                v = np.transpose(u/nl.norm(u))
                v_opencl    = p.Vector(v)
		#erzeuge neue Householder Matrix
                Qk_opencl = v_opencl.outer(v_opencl) * np.float32(2.0)
                #Qk_opencl = p.Matrix(np.outer(v,np.transpose(v))) 
		Qk_opencl = I_opencl - p.Matrix(Qk_opencl.value)
                
                #Qk_opencl.execute()
                
		Q_opencl = Q_opencl * Qk_opencl
		A_opencl = Qk_opencl * A_opencl
                #Q_opencl.execute()
                #A_opencl.execute()
                A = A_opencl.value
                
	return Q_opencl,A_opencl

def loesen_mit_qr_gpu(A,y):
	A = np.float32(np.array(A))
	n,m = np.shape(A)

	I = np.float32(np.eye(n,m))
	Q = np.float32(np.eye(n,m))
	
        I_opencl    = p.Matrix(I)
        Qk_opencl   = p.Matrix(I)
        Q_opencl    = p.Matrix(Q)
        A_opencl    = p.Matrix(A)
        y_opencl    = p.Vector(np.float32(np.array(y)))

	for k in range (n-1): #Algorythmus bricht bei k=n-1 zusammen, da dann letzte Zeile nur ein Element beihnhaltet, das Projeziert 0 ist
		x = np.transpose(np.copy(A[:,[k]]))[0]
		x[:k] = 0.0
		e = np.transpose(I[:,[k]])[0]
		
		alpha = -np.sign(x[k])*nl.norm(x)		
                u = x+alpha*e	
                v = np.transpose(u/nl.norm(u))
                v_opencl    = p.Vector(v)

		#erzeuge neue Householder Matrix
                Qk_opencl = v_opencl.outer(v_opencl) * np.float32(2.0)
		Qk_opencl = I_opencl - p.Matrix(Qk_opencl.value)
                
                
		Q_opencl = Q_opencl * Qk_opencl
		A_opencl = Qk_opencl * A_opencl
                A = A_opencl.value

        y_opencl = p.Trans(Q_opencl.execute()) * y_opencl
        y_opencl = p.solve(A_opencl.execute(),y_opencl.execute(),p.upper_tag())
	return y_opencl

def loesen_mit_qr(A,y):
	Q,R = qr_zerlegung(A)
	x = sl.solve_triangular(R,np.dot(Q,y))
#	x = rueckwaerts(R,np.dot(Q,y))
	return x

if __name__ == "__main__":
	#Testmatix aus Internetquelle bzw. Wikipediaartikel
	A = [[12, -51, 4], [6, 167, -68], [-4, 24, -41]]

	Q,R = qr_zerlegung(A)
	Q_op,R_op = qr_zerlegung_opencl(A)
	Q2,R2 = sl.qr(A)

	print "Q"
	print Q
        print Q_op
	print Q2, '\n'
	print "R"
	print R
        print R_op
	print R2,'\n'
