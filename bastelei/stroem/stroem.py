import numpy as np
import numpy.linalg as nl
import matplotlib as mp
mp.rcParams['figure.max_open_warning'] = 40
backend =  mp.get_backend()
if backend == 'agg':
	mp.use('TkAgg')
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from matplotlib.colors import SymLogNorm
import scipy as sp
from pylab import *

def erstelle_Matrix(Pe=14, dt=100.0, N=10):
    dx  = 1.0/float(N+1)
    s   = dt*Pe/(2.0*dx)
    k   = dt/(dx*dx)
    nn  = (N+1)*(N+1)
    na  = N+1
    A   = np.zeros((nn,nn))
    x   = np.linspace(0,1,na)
    y   = np.linspace(0,1,na)
    vx  = np.outer(sin(2.0*pi*x),cos(pi*y))*pi
    vy  = -1*np.outer(cos(2.0*pi*x),sin(pi*y))*2.0*pi

    for i in range(na):
        for j in range(na):
            A[i*na+j][i*na+j] = 1.0*4.0*k
            if j==0 or j==N:
                A[i*na+j][i*na+j] = 1.0
            else:
                A[i*na+j][i*na+j-1] = -(s*vy[i][j]+k)
                A[i*na+j][i*na+j+1] = s*vy[i][j]-k
            if i==0 and j != 0 and j != N:
                A[i*na+j][(i+1)*na+j] = -2.0*k
            elif i==N and j != 0 and j != N:
                A[i*na+j][(i-1)*na+j] = -2.0*k
            elif j != 0 and j != N:
                A[i*na+j][(i-1)*na+j] = -(s*vx[i][j]+k)
                A[i*na+j][(i+1)*na+j] = (s*vx[i][j]-k)
    return A

def erstelle_Vektor(T,Pe=0.7,N=10):
	na  = N+1
	nn  = na*na
	x   = np.zeros(nn)
	for i in range(na):
		for j in range(na):
			x[i*na+j] = T[i][j]
	return x

def startwerte(N=10,Pe=0.7):
	T = np.zeros((N+1,N+1))
	for i in range(N+1):
		for j in range(N+1):
			T [i][j] = j/float(N)
	return T

def hole_aus_vektor(x,N=10):
	T   = np.zeros((N+1,N+1))
	for i in range(N+1):
            for j in range(N+1):
               T[i][j]	= x[i*(N+1)+j]
	return T

if __name__ == "__main__":
	N	= 50
	T	= startwerte(N=N)
	x	= erstelle_Vektor(T,N=N)
	A	= erstelle_Matrix(N=N)
	y	= nl.solve(A,x)
	T	= hole_aus_vektor(y,N=N)
	#print T
	fig = plt.figure()
	plt.imshow(np.transpose(T), interpolation='none', aspect='equal', origin='lower')
	plt.colorbar()
	plt.show()
