"""
Modul zur implementation der Loesungsverfahren.
Nur diese Datei muss eingebunden werden.
Nur die Routine loesen() muss ausgefuehrt werden.
Eine hilfe ist mit der Routine help_loesen() vorhanden.

Author: Kevin Luedemann
EMail: kevin.luedemann@stud.uni-goettingen.de
"""

import numpy as np
import numpy.linalg as nl
import scipy as sp
import scipy.linalg as sl
import matplotlib
#matplotlib.use('TkAgg')
from pylab import *
import matplotlib.pyplot as plt
import sys
import time as ti

# Importpath der eigenen Module zum Loesen
sys.path.append("../lu/")
sys.path.append("../qr/")
sys.path.append("../gaussseidel/")

sys.path.append("../pyviennacl/lu/")
sys.path.append("../pyviennacl/qr/")
sys.path.append("../pyviennacl/gaussseidel/")

# Eigene Module zum loesen
import lu_vektor as luit
import qr_iterativ as qrit
import gaussseidel_jacobi_iterativ as gsjit

import lu_opencl as luop
import qr_opencl as qrop
import gaussseidel_jacobi_opencl as gsjop

def help_loesen():
	print """Das Programm loesen() loest die lineare Gleichung Ax=y mit verschiedenen Verfahren.\n
Hierbei werden verwendet:\n
1. LU bzw. PLU: Eine LU Zerlegung mit Pivotisierung
2. QR bzw. PQR: Eine QR Zerlgeung mit Pivotisierung
3. J: Das Jacobi bzw. Gesammtschrittverfahren mit Pivotisierung
4. GS: Das Gauss-Seidel bzw. Einzelschrittverfahren
\n
Der Programmaufruf sieht wie folgt aus:\n
x = loesen(A,y,parallel=False,loeser=\"GS\")\n
Die ersten beiden Parameter sind die Matrix A und der Loesungsvektor y.
Der naechste Parameter \"parallel\" ist ein schalten fuer die Prallele Berechnung
Momentan steht nur die Berechnung per OpenCL zur verfuegung.\n
Der letzte Parameter waehlt das Loesungsverfahren aus den oben genannten."""

# Programm aus Vorlesung Wissenschaftliches Rechnen mit Python uebernommen
# Dies dient als Basistestroutine zum validieren der Parallelsisierung
# Mit dieser Routine werden die Matrix und die Vektoren erstellt,
# welche anschliessend mit den jeweiligen Loesungsverfahren und den jeweiligen parallelierungen evaluiert werden.
def randwertaufgabe(n):
	"""
	berechnet mit Finiten Differenzen die Loesung u von
	-u''=f in (0,1), u(0)=u(1)=0
	n =  Anzahl Stuetzstellen      
	@author Gerd Rapin, Jochen Schulz
	"""
	# Erzeugen des Gitters
	x = linspace(0,1,n)
	x_i = x[1:n-1]
	# Aufstellen des lin. Gls.
	A = diag(2*ones(n-2),0)+diag(-1*ones(n-3),-1)+diag(-1*ones(n-3),1)
	F = (1./n)**2*exp(x_i) # rechte Seite fuer f=exp(x) 
	return (A,F)

def loesen(A,y,parallel=False,loeser="GS"):
	n,m = np.shape(A)
	x = np.zeros(n)
	
	#pivotisierung nur falls noetig
	if loeser == "LU" or loeser == "PLU" or loeser == "QR"  or loeser == "PQR"or loeser == "J":
		P = luit.pivotmatrix(A)
		A = np.dot(P,A)
		y = np.dot(P,y)

	#waehlen des Loesungsverfahrens
	if parallel:
		#Wahl der Verfahrens
		if loeser == "LU" or loeser == "PLU":
			x = luop.loesung_mit_lu_gpu(A,y)
		elif loeser == "QR" or loeser == "PQR":
			x = qrop.loesen_mit_qr_gpu(A,y)
		elif loeser == "GS":
			x = gsjop.gaussseidel_einzelschritt_opencl(A,y,toll=1e-7)
		elif loeser == "J":
			x = gsjop.jacobi_gesammtschritt_opencl(A,y,toll=5e-3)
		#Verfahren nicht implementiert
		else:
			assert False, "loeser: \"" + loeser + "\" nicht implementiert"
	else:
		#Wahl der Verfahrens
		if loeser == "LU" or loeser == "PLU":
			x = luit.loesung_mit_lu(A,y)
		elif loeser == "QR" or loeser == "PQR":
			x = qrit.loesen_mit_qr(A,y)
		elif loeser == "GS":
			x = gsjit.gaussseidel_einzelschritt(A,y)
		elif loeser == "J":
			x = gsjit.jacobi_gesammtschritt(A,y)
		#Verfahren nicht implementiert
		else:
			assert False, "loeser: \"" + loeser + "\" nicht implementiert"
	return x

if __name__ == "__main__":
	N = 1e1
	A,y = randwertaufgabe(N)
	start = ti.time()
	z_lu = loesen(A,y,parallel=False,loeser="LU")
	stopluit = ti.time()
        print "iterativ:"
	print "LU:\t" + "{}".format(stopluit-start)
	z_gs	= loesen(A,y,loeser="GS")
	stopgsit	= ti.time()
	print "GS:\t" + "{}".format(stopgsit-stopluit)
	z_j	= loesen(A,y,loeser="J")
	stopjit	= ti.time()
	print "J:\t" + "{}".format(stopjit-stopgsit)
	z_qr	= loesen(A,y,loeser="QR")
	stopqrit	= ti.time()
	print "QR:\t" + "{}".format(stopqrit-stopjit)
	z_sp	= sl.solve(A,y)
	stopsp	= ti.time()
	print "SCIPY:\t" + "{}".format(stopsp-stopqrit)
	z_np	= nl.solve(A,y)
	stopnp	= ti.time()
	print "NUMPY:\t" + "{}".format(stopnp-stopsp)

        print "PyViennacl:"
	z_lu = loesen(A,y,parallel=True,loeser="LU")
        z_lu = z_lu.value
	stopluop = ti.time()
	print "LU:\t" + "{}".format(stopluop-stopnp)
	z_gs	= loesen(A,y,parallel=True,loeser="GS")
        z_gs = z_gs.value
	stopgsop	= ti.time()
	print "GS:\t" + "{}".format(stopgsop-stopluop)
	z_j	= loesen(A,y,parallel=True,loeser="J")
        z_j = z_j.value
	stopjop	= ti.time()
	print "J:\t" + "{}".format(stopjop-stopgsop)
	#z_qr	= loesen(A,y,parallel=True,loeser="QR")
	stopqrop	= ti.time()
	print "QR:\t" + "{}".format(stopqrop-stopjop)

        """
	# Darstellen der Loesung
	# Dies wird nur genutzt, um die Loesung anschliessen auf Richtigkeit zu ueberpruefen
	x = np.linspace(0,1,N)
	z = hstack((0, z_lu, 0))
	plt.plot(x,z,'r*-', label="lu")
	z = hstack((0, z_gs, 0))
	plt.plot(x,z,'b*-', label="gs")
	z = hstack((0, z_j, 0))
	plt.plot(x,z,'y*-', label="j")
	z = hstack((0, z_qr, 0))
	plt.plot(x,z,'g*-', label="qr")
	z = hstack((0, z_sp, 0))
	plt.plot(x,z,'m*-', label="sp")
	z = hstack((0, z_np, 0))
	plt.plot(x,z,'c*-', label="np")
        plt.legend()
	plt.show()
        """
