import numpy as np
import numpy.linalg as nl
import scipy as sp
from pylab import *
import matplotlib.pyplot as plt
import sys

# Importpath der eigenen Module zum Loesen
sys.path.append("../lu/")
sys.path.append("../qr/")

# Eigene Module zum loesen
import lu_vektor as luit
import qr_iterativ as qrit

# Programm aus Vorlesung Wissenschaftliches Rechnen mit Python uebernommen
# Dies dient als Basistestroutine zum validieren der Parallelsisierung
# Mit dieser Routine werden die Matrix und die Vektoren erstellt,
# welche anschliessend mit den jeweiligen Loesungsverfahren und den jeweiligen parallelierungen evaluiert werden.
def randwertaufgabe(n):
	"""
	berechnet mit Finiten Differenzen die Loesung u von
	-u''=f in (0,1), u(0)=u(1)=0
	n =  Anzahl Stuetzstellen      
	@author Gerd Rapin, Jochen Schulz
	"""
	# Erzeugen des Gitters
	x = linspace(0,1,n)
	x_i = x[1:n-1]
	# Aufstellen des lin. Gls.
	A = diag(2*ones(n-2),0)+diag(-1*ones(n-3),-1)+diag(-1*ones(n-3),1)
	F = (1./n)**2*exp(x_i) # rechte Seite fuer f=exp(x) 
	
	return (A,F)

if __name__ == "__main__":
	N = 1e2
	(A,y) = randwertaufgabe(N)

	z_i = luit.loesung_mit_lu(A,y)
	#z_i = qrit.loesen_mit_qr(A,y)


	# Darstellen der Loesung
	# Dies wird nur genutzt, um die Loesung anschliessen auf Richtigkeit zu ueberpruefen
	z = hstack((0, z_i, 0))
	fig = figure()
	x = np.linspace(0,1,N)
	plot(x,z,'r*-')
	plt.show()

