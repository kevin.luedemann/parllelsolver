function x=newton(F,x0)

% newton.m
%-------------
% Implementation des Newton Verfahrens mit heuristischem Abbruchkriterium
% Input: - Struktur F mit Feldern --F.evalderivative
%                                 --F.evalfunctionvalue
%                                 --F.kmax
%                                 --F.epsilon
%        - Startwert x0
% Output: Schritte des Newtonverfahrens
%-------------
% Frederic Weidling 12.01.16

x=x0(:);
k=0;

% Erster Schritt des Newtonverfahrens
Deltax=-F.evalderivative(x(:,1))\F.evalfunctionvalue(x(:,1));
x(:,2)=x(:,1)+Deltax;
normDelta_new=norm(Deltax); %Speichern für später

stopparameter=1;
q=0;

while stopparameter
    % Parameter update
    k=k+1;
    normDelta_old=normDelta_new;
    
    % Schritt des Verfahrens
    Deltax=-F.evalderivative(x(:,k+1))\F.evalfunctionvalue(x(:,k+1)); %für große Systeme sollte macn aich selbst Gedanken machen, wie man das System am besten löst
    x(:,k+2)=x(:,k+1)+Deltax;
    
    % Heuristisches Abbruchkriterium nach Banach
    normDelta_new=norm(Deltax);
    q= max(normDelta_new/normDelta_old,q);
    if (q>=1)
        disp('Verfahren scheint nicht zu konvergieren')
        break
    end
    
    if ((k>=F.kmax)||(q/(1-q)*normDelta_new<=F.epsilon))
        stopparameter=0;
    end
end
