function F=beispiel()
% funktion401.m
%-------------
% Stellt eine Struktur zum Auswerten von Funktion und Ableitung von
% f(x)=x-exp(-x) bereit
%-------------
% Output: Struktur F
%-------------
% Frederic Weidling 12.01.16
    F.epsilon=10^(-10);
    F.kmax=100;

    F.evalfunctionvalue=@evalfunctionvalue;
    F.evalderivative=@evalderivative;
end

function y=evalfunctionvalue(x)
    % Auswertung von f'
    y=x-exp(-x);
end

function y=evalderivative(x)
    % Auswertung von f'
    y=1+exp(-x);
end
