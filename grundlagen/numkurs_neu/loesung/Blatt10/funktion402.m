function F=funktion402()
% funktion402.m
%-------------
% Stellt eine Struktur zum Auswerten von Funktion und Ableitung von
% f(x)=x^4*cos(x) bereit, die so abgeändert wurde, dass das modifizierte
% Newtonverfahren zum Auffinden der mehrfachen Nullstelle bei x=0
% durchgeführt wird.
%-------------
% Output: Struktur F
%-------------
% Frederic Weidling 12.01.16

    F.epsilon=10^(-10);
    F.kmax=100;

    F.evalfunctionvalue=@evalfunctionvalue;
    F.evalderivative=@evalderivative;
end

function y=evalfunctionvalue(x)
    % Auswertung der Funktion m*f
    y=4*x^4*cos(x);
end

function y=evalderivative(x)
    % Auswertung von f'
    y=4*x^3*cos(x)-x^4*sin(x);
end
