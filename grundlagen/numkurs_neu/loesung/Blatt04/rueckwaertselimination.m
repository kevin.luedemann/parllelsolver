function [ x ] = rueckwaertselimination( U,b )

% rueckwaertselimination.m
%
% Berechnet die Loesung von Ux=b mit einer obere Dreiecksmatrix L.
% Input:
% U obere Dreiecksmatrix
% b rechte Seite (Vektor passender Laenge)
% Output:
% x Loesung des LGS
%
% Frederic Weidling 16.11.15

% Transformation des Systems in ein System der Form Lx=b' mit L unterer
% Dreiecksmatrix
L=rot90(U,2);
b=rot90(b(:),2);

% Berechnung der Lösung mittels vormartselimination.m
x= vorwaertselimination(L,b);

% Lösung "zurückdrehen"
x=rot90(x,2);

end

