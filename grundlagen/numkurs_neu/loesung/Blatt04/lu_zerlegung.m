function [L,U]=lu_zerlegung(T)

% lu_zerlung.m
%
% Berechnet zur Matrix T die LU Zerlegung mittels Gausselimination
% Input:
% T quadratische Matrix
% Output:
% L untere Dreiecksmatrix
% U obere Dreiecksmatrix
%
% Frederic Weidling, 16.11.15

% Ist T quadratisch?
[n,m]=size(T);
if n~=m
    error('T ist nicht quadratisch')
end

for kkk=1:n-1
    % Ist der aktuelle Schritt durchführbar?
    if T(kkk,kkk)==0
        error('Nulleintrag auf Diagonale erhalten, Verfahren nicht durchführbar')
    end
    
    % Durchführung des k-ten Schrittes
    l=T(kkk+1:n,kkk)/T(kkk,kkk);
    T(kkk+1:n,kkk)=l;
    T(kkk+1:n,kkk+1:n)=T(kkk+1:n,kkk+1:n)-l*T(kkk,kkk+1:n);
end
% Ist der letzte Schritt okay?
if T(n,n)==0
    error('Nulleintrag auf Diagonale erhalten, Verfahren nicht durchführbar')
end

U=triu(T);
L=tril(T,-1)+eye(n);
