function [ x ] = lu_solve( T,b )

% lu_solve.m
%
% Löst das Gleichungssystem Tx=b mittels Gauß
% Input:
% T quadratische Matrix
% b rechte Seite
% Output:
% x Lösung des Gleichungssystems

% Berechnung der Zerlegung
[L,U]=lu_zerlegung(T);

% Löse Ly=b
y = vorwaertselimination(L,b);

% Löse Ux=y
x = rueckwaertselimination(U,y);

end

