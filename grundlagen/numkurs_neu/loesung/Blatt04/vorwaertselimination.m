function x= vorwaertselimination(L,b)

% vorwartselimination.m
%
% Berechnet die Loesung von Lx=b mit einer unteren Dreiecksmatrix L.
% Input:
% L untere Dreiecksmatrix
% b rechte Seite (Vektor passender Laenge)
% Output:
% x Loesung des LGS
%
% Frederic Weidling 16.11.15

% Check ob Matrix invertierbar ist
if ismember(0,diag(L))
    error('Die Matrix ist nicht invertierbar')
end

% b zu Spaltenvektor machen
b=b(:);

% Abfrage ob Parameter zueinander passende Größe haben
[N1,N2]=size(L);
N3= size(b);
if N1 ~= N2
    error('Eingabematrix nicht quadratisch')
elseif N1~=N3
    error('Die Dimensionen der Matrix und des Vektors stimmen nicht ueberein!');
elseif ~istril(L)
   error('L ist keine untere Dreiecksmatrix') 
end

% Reservierung von Speicherplatz
x=zeros(size(b));

% Eliminationsverfahren
for iii=1:N1
    l=L(iii,:);
    x(iii)=b(iii)-l*x;
    x(iii)=x(iii)/L(iii,iii); 
end
 