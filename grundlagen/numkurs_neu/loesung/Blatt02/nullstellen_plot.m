% Plottet zwei Varianten der Anwendung des Banachschen Fixpunktsatzes zur Lösung von 2x=tan(x)

n=10;

x=zero_g1(n);
y=zero_g2(n);

subplot(2,1,1)
plot([0:n-1],x,'x-')
title('Nullstellenapproximation mittels arctan(2x)')
xlabel('Interation n')
ylabel('Approximation')

subplot(2,1,2)
plot([0:n-1],y,'x-')
title('Nullstellenapproximation mittels 1/2 tan(x)')
xlabel('Interation n')
ylabel('Approximation')
