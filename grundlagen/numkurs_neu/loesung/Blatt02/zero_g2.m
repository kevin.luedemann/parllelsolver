function [ x ] = zero_g2(n)

% zero_g2.m
%
% Ausgabe der ersten Schritte des Banachfixpunktverfahrens für
% g(x)=1/2 tan(x)
% Input: Anzahl Interation n 
% Output: Vektor x der interierten

% Reservierung des Speicherplatzes und setzen des Startwertes
x=zeros(n,1);
x(1)=1;

% Interation des Verfahrens
for iii=2:n
   x(iii)=1/2*tan(x(iii-1)); 
end


end

