function [ x ] = zero_g1(n)

% zero_g1.m
%
% Ausgabe der ersten Schritte des Banachfixpunktverfahrens für
% g(x)=arctan(2x)
% Input: Anzahl Interation n 
% Output: Vektor x der interierten

% Reservierung des Speicherplatzes und setzen des Startwertes
x=zeros(n,1);
x(1)=1;

% Interation des Verfahrens
for iii=2:n
   x(iii)=atan(2*x(iii-1)); 
end


end

