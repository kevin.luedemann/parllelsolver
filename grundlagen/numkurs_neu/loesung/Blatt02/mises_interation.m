function [ rho ] = mises_interation( A,x )

% mises_interation
% Durchführung der Mises Interation zur numerischen Bestimmung der 2-Norm von A
% Input: Matrix A, Vektor x
% Output: rho, the norm of A
% Frederic Weidling, 04.11.15

B=A'*A;

% Gewähltes Abbruchkriterium ist die Änderung von rho um weniger als epsilon
rho=0;
delta_rho=1;
epsilon=1e-6;

while delta_rho>epsilon
    old_rho=rho;
    
    y=B*x;
    x=y/norm(y,2);
   
    rho=norm(y,2)^(1/2);
    delta_rho=abs(old_rho-rho);
end
end

