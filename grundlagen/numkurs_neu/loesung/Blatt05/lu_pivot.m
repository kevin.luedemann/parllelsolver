function [L,U,P]=lu_pivot(T)

% lu_zerlung.m
%
% Berechnet zur Matrix T die LU Zerlegung mittels Gausselimination und
% Pivotisierung
% Input:
% T quadratische Matrix
% Output:
% L untere Dreiecksmatrix
% U obere Dreiecksmatrix
% P Permutationsmatrix
%
% Frederic Weidling, 27.11.15

    % Ist T quadratisch?
    [n,m]=size(T);
    if n~=m
        error('T ist nicht quadratisch')
    end
    % Variable die Pivotisierung speichert
    p=1:n;

    for kkk=1:n-1
        % Finden des Pivotindex
        [~,max_element]=max(abs(T(kkk:end,kkk)));
        max_element=max_element-1+kkk;
        
        % Schritt durchführbar?
        if T(max_element,kkk)==0
            error('0 als maximaler Eintrag, Verfahren nicht durchführbar')
        end
        
        %Pivotisieren falls nötig
        if max_element ~= kkk
            p([kkk, max_element])=p([max_element, kkk]);
            T([kkk, max_element],:)=T([max_element, kkk],:);
        end

        % Durchführung des k-ten Schrittes
        l=T(kkk+1:n,kkk)/T(kkk,kkk);
        T(kkk+1:n,kkk)=l;
        T(kkk+1:n,kkk+1:n)=T(kkk+1:n,kkk+1:n)-l*T(kkk,kkk+1:n);
    end
    % Ist der letzte Schritt okay?
    if T(n,n)==0
        error('Nulleintrag auf Diagonale erhalten, Verfahren nicht durchführbar')
    end

    U=triu(T);
    L=tril(T,-1)+eye(n);
    
    %Erstellen der Permutationsmatrix
    P=eye(n);
    P=P(p,:);
end

