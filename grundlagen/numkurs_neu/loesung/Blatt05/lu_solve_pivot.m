function [ x ] = lu_solve_pivot( T,b )

% lu_solve_pivot.m
%
% Löst das Gleichungssystem Tx=b mittels Gauß und Pivotisierung
% Input:
% T quadratische Matrix
% b rechte Seite
% Output:
% x Lösung des Gleichungssystems

% Berechnung der Zerlegung
[L,U,P]=lu_pivot(T);
% Berechne neue rechte Seite
b=P*b;
% Löse Ly=b
y = vorwaertselimination(L,b);

% Löse Ux=y
x = rueckwaertselimination(U,y);

end

