% squarezeit.m
%
% Vergleicht die Laufzeiten zweier verschiedene Varianten die eintragsweisen Quadrate 
% von großen Vektoren zu berechen
%
% Frederic Weidling, 23.10.2015


N=[1e4,1e5,1e6,1e7];
zeit=zeros(length(N),2);

for iii=1:length(N)
    x=[1:N(iii)];
    for jjj=1:10
        y=zeros(size(x)); % Reservierung des Speichers und Löschen der Ergebnisse
        z=zeros(size(x));
        
	% Messung Variante 1
        tic
        for kkk=1:length(x)
            y(kkk)=x(kkk)^2;
        end
        t=toc;
        zeit(iii,1)=zeit(iii,1)+t;

	% Messung Variante 2    
        tic
        z=x.^2;
        t=toc;
        zeit(iii,2)=zeit(iii,2)+t;

    end
end

% Auftragen der Ergebnisse 
loglog(N, zeit(:,1), '-s',N, zeit(:,2),'-s')
