function A=small_to_zero(A)

% small_to_zero.m
%
% setzt kleine Einträge in einer Testmatrix auf 0
% Input: Matrix A
% Output: Matrix A
%
% Frederic Weidling 23.10.15


indices_to_zero=find(abs(A)<=0.5);
A(indices_to_zero)=0;
