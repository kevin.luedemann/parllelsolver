function [ x ] = zero_g1(n)

% zero_g1.m
%
% Ausgabe der ersten Stellen des Banachfixpunktverfahrens für
% g(x)=arctan(2x)
% Input: Anzahl Interation n 
% Output: Vektor x der interierten

% Reservierung des Speicherplatzes und setzen des Startwertes
x=zeros(n,1);
x(1)=x_0;

% Interation des Verfahrens
for iii=2:n
   x(iii)=1/(1+sqrt(1+x(iii-1))); 
end


end

