% ellipse.m
% Liest die in data24.mat gespeicherten Vektoren ein, und fittet Parameter
% einer Ellipse an die durch diese Werte gegebenen Datenpunkte, das
% Ergebnis wird graphisch ausgegeben
% Frederic Weidling, 1.12.15

% Einlesen der Daten
load('data24.mat');

% Aufstellen und lösen der Gleichungssysteme
X=[xtrue,x1,x2,x3];
Y=[ytrue,y1,y2,y3];

solutions=zeros(5,4);

onecolumn=ones(size(xtrue));
for jjj=1:4
    A=[Y(:,jjj).^2,X(:,jjj).*Y(:,jjj),X(:,jjj),Y(:,jjj), onecolumn];
    b=X(:,jjj).^2;
    
    % Normalengleichung und lösen
    Atilde=A'*A;
    btilde=A'*b;

    solutions(:,jjj)=Atilde\btilde;
end

% Erstellen der Function handels für ezplot
F= { @(x,y) -x.^2+solutions(1,1)*y.^2+solutions(2,1)*x.*y+solutions(3,1)*x+solutions(4,1)*y+solutions(5,1); ...
     @(x,y) -x.^2+solutions(1,2)*y.^2+solutions(2,2)*x.*y+solutions(3,2)*x+solutions(4,2)*y+solutions(5,2); ...
     @(x,y) -x.^2+solutions(1,3)*y.^2+solutions(2,3)*x.*y+solutions(3,3)*x+solutions(4,3)*y+solutions(5,3); ...
     @(x,y) -x.^2+solutions(1,4)*y.^2+solutions(2,4)*x.*y+solutions(3,4)*x+solutions(4,4)*y+solutions(5,4) };
 
 % Plotten der Ergebnisse
 for jjj=0:1
     for kkk=1:2
         lll=(jjj*2+kkk);
         subplot(2,2,lll)
         hold on
         ezplot(F{lll})  
         plot(X(:,lll),Y(:,lll),'x')
         hold off
     end
 end
