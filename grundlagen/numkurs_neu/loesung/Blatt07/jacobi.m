function [ x,k ] = jacobi( A,b,x0,epsilon,maxit )

% jacobi.m
% Implementation des Jacobiverfahrens zur Lösung von Ax=b
% Input:
% Matrix A
% Vektoren b,x
% Zahlen epsilon, maxit
% Output:
% Lösung x
% Schrittzahl k
% Frederic Weidling, 07.12.15

% Default values
if (nargin<5)
    maxit=100;
end
if (nargin<4)
    epsilon=10^(-10);
end
if (nargin<3)
    x0=zeros(size(b));
end

% Konsistenzcheck
x=x0(:);
b=b(:);
n=[length(x),length(b),size(A)];

if ~(all(n == n(1)))
    error('Dimensionen des Gleichungssystems passen nicht zusammen')
end

% Ausrechnen aller am Anfang berechnbarer Größen
d=diag(A);
Dinv=spdiags(d.^(-1),0,length(d),length(d));

btilde=Dinv*b;
J=Dinv*(A-diag(d));

k=0;
xalt=x;
stop=1;

while stop
   %Schritt des Algorithmus
   x=btilde-J*x;
   
   % Stopkriterien überprüfen
   k=k+1;
   steplength=norm(x-xalt)/(length(x));   
   if ((k>=maxit) || (steplength<epsilon))
       stop=0;
   end
   xalt=x;
end

end