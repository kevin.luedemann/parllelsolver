% interativepoissonsolve.m
% Löst die Poissongleichung mittels Jacobi und Gauss-Seidel und vergleicht
% Laufzeit und Interationszahl
%Frederic Weidling, 08.12.15

n=100*[1:6];
iter=zeros(2,length(n));
t=zeros(2,length(n));

for jjj=1:length(n)
    % Erstellen des Gleichungssystems
    A=spdiags(ones(n(jjj),1)*[-1 2 -1],-1:1,n(jjj),n(jjj));
    rhs=ones(length(A),1);
    x0=zeros(size(rhs));
    
    %Lösen mit Jacobi
    tic
    [~,k]=jacobi(A,rhs,x0,10^-5/n(jjj),10^6);
    t(1,jjj)=toc;
    iter(1,jjj)=k;

    %Lösen mit Gauß-Seidel
    tic
    [~,k]=gaus_seidel(A,rhs,x0,10^-5/n(jjj),10^6);
    t(2,jjj)=toc;
    iter(2,jjj)=k;
end

subplot(1,2,1)
plot(n,t(1,:),n,t(2,:))
legend('Jacobi','Gauß-Seidel')
xlabel('Größe der Matrix')
ylabel('Zeit')

subplot(1,2,2)
plot(n,iter(1,:),n,iter(2,:))
legend('Jacobi','Gauß-Seidel')
xlabel('Größe der Matrix')
ylabel('Interationen')