function [ x,k ] = gaus_seidel( A,b,x0,epsilon,maxit )

% gaus_seidel.m
% Implementation des Gauß-Seidel-Verfahrens zur Lösung von Ax=b
% Input:
% Matrix A
% Vektoren b,x
% Zahlen epsilon, maxit
% Output:
% Lösung x
% Schrittzahl k
% Frederic Weidling, 07.12.15

% Default values
if (nargin<5)
    maxit=100;
end
if (nargin<4)
    epsilon=10^(-10);
end
if (nargin<3)
    x0=zeros(size(b));
end

% Konsistenzcheck
x=x0(:);
b=b(:);
n=[length(x),length(b),size(A)];

if ~(all(n == n(1)))
    error('Dimensionen des Gleichungssystems passen nicht zusammen')
end

% Ausrechnen aller am Anfang berechnbarer Größen
DplusL=tril(A);
U=triu(A,1);

k=0;
xalt=x;
stop=1;

while stop
   %Schritt des Algorithmus
   x=DplusL\(b-U*x);
   
   % Stopkriterien überprüfen
   k=k+1;
   steplength=norm(x-xalt)/(length(x));   
   if ((k>=maxit) || (steplength<epsilon))
       stop=0;
   end
   xalt=x;
end

end