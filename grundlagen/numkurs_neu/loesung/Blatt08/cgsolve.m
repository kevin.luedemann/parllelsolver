function [ x,k ] = cgsolve( A,b,x0,epsilon,maxit )

% cgsolve.m
% Implementation des CG-Verfahrens zur Lösung von Ax=b
% Input:
% Matrix A
% Vektoren b,x
% Zahlen epsilon, maxit
% Output:
% Lösung x
% Schrittzahl k
% Frederic Weidling, 07.12.15

% Default values
if (nargin<5)
    maxit=length(b);
end
if (nargin<4)
    epsilon=10^(-5);
end
if (nargin<3)
    x0=zeros(size(b));
end

% Konsistenzcheck
x=x0(:);% Interierter
b=b(:);
n=[length(x),length(b),size(A)];

if ~(all(n == n(1)))
    error('Dimensionen des Gleichungssystems passen nicht zusammen')
end

% Ausrechnen aller am Anfang berechnbarer Größen

r=b-A*x; % Residuum
d=r; %Suchrichtung

ressize=r'*r; %norm des Residuum^2

k=0;
epsilon=epsilon^2; % da sonst nur Normquadrat verwendet

% Ist die Anfangslösung gut genug?
if ressize>epsilon
    stop=1;
else
    stop=0;
end

while stop
   %Schritt des Algorithmus
   help=A*d;
   alpha=ressize/(d'*help);
   x=x+alpha*d;
   r=r-alpha*help;
   newressize=r'*r;
   beta=newressize/ressize;
   d=r+beta*d;
   
   % Stopkriterien überprüfen
   k=k+1;
   if ((k>=maxit) || (ressize<epsilon))
       stop=0;
   else
       ressize=newressize;
   end
end

end