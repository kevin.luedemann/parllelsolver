% poissonsolvecg.m
% Löst die Poissongleichung mittels Cg und bestimmt Interationen und
% Laufzeit
%Frederic Weidling, 08.12.15

n=100*[1:6];
iter=zeros(1,length(n));
t=zeros(1,length(n));

for jjj=1:length(n)
    % Erstellen des Gleichungssystems
    A=spdiags(ones(n(jjj),1)*[-1 2 -1],-1:1,n(jjj),n(jjj));
    rhs=ones(length(A),1);
    x0=zeros(size(rhs));
    
    %Lösen mit CG
    tic
    [~,k]=cgsolve(A,rhs,x0,10^-5/n(jjj),10^6);
    t(jjj)=toc;
    iter(jjj)=k;
end

subplot(1,2,1)
plot(n,t)
legend('CG')
xlabel('Größe der Matrix')
ylabel('Zeit')

subplot(1,2,2)
plot(n,iter)
legend('CG')
xlabel('Größe der Matrix')
ylabel('Interationen')