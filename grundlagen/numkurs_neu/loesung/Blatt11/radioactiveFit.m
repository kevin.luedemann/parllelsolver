function struc=radioactiveFit()
% radioactiveFit.m
%-------------
% Stellt eine Struktur für die Mischung zweier radioaktiver Substanzen
% bereit
%-------------
% Output: Struktur struc
%-------------
% Frederic Weidling 19.01.16
    struc.EPSILON=10^(-4);
    struc.KMAX=100;
    struc.evalFunction=@evalFunction;
    struc.evalDerivative=@evalDerivative;
    
    filename='data44.mat';
    load(filename);
    struc.t=t;
    struc.b=intensity;
end

function y=evalFunction(x,struc)
    % Auswertung von f'
    y=x(1)*exp(-x(3)*struc.t)+x(2)*exp(-x(4)*struc.t)-struc.b;
    y=y(:);
end

function y=evalDerivative(x,struc)
    % Auswertung von f'
    y1=exp(-x(3)*struc.t);
    y2=exp(-x(4)*struc.t);
    y3=-x(1)*struc.t.*y1;
    y4=-x(2)*struc.t.*y2;
    y=[y1(:),y2(:),y3(:),y4(:)];
end
