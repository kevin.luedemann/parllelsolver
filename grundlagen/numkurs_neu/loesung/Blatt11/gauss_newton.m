function x=gauss_newton(struc,x0)

% newton.m
%-------------
% Implementation des Newton Verfahrens mit heuristischem Abbruchkriterium
% Input: - Struktur struc mit Feldern --struc.evalDerivative
%                                     --struc.evalFunction
%                                     --struc.KMAX
%                                     --struc.EPSILON
%        - Startwert x0
%        - rechte Seite b
% Output: Schritte des Gauss-Newton Verfahrens
%-------------
% Frederic Weidling 12.01.16

x=x0(:);
k=0;

% Erster Schritt des Gauss-Newton Verfahrens
% Aufstellen des linearen Ausgleichproblems ||A*x-rhs||^2
rhs=struc.evalFunction(x,struc);
A=struc.evalDerivative(x,struc);

% Lösen des linearen Ausgleichproblems
[newRhs,R] = qr(sparse(A),rhs);
Deltax=-R\newRhs;

x(:,2)=x(:,1)+Deltax;
normDelta_new=norm(Deltax); %Speichern für später

stopparameter=1;
q=0;

while stopparameter
    % Parameter update
    k=k+1;
    normDelta_old=normDelta_new;
    
    % Aufstellen des linearen Ausgleichproblems ||Ax-rhs||^2
    rhs=struc.evalFunction(x(:,k+1),struc);
    A=struc.evalDerivative(x(:,k+1),struc);

    % Lösen des linearen Ausgleichproblems
    [newRhs,R] = qr(sparse(A),rhs);
    Deltax=-R\newRhs;
    
    x(:,k+2)=x(:,k+1)+Deltax;
    
    % Heuristisches Abbruchkriterium nach Banach
    normDelta_new=norm(Deltax);
    q= max(normDelta_new/normDelta_old,q);
    if (q>=1)
        disp('Verfahren scheint nicht zu konvergieren')
        break
    end
    
    if ((k>=struc.KMAX)||(q/(1-q)*normDelta_new<=struc.EPSILON))
        stopparameter=0;
    end
end
