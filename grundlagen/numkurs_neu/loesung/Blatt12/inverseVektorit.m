function [ lambda , x ] = inverseVektorit (A , lambda0 , x0 , N )

% inverseVektorit.m
%--------------------
% inverse Vektoriteration zur Berechnung eines Eigenwertes
% lambda von A und des zugehoerigen Eigenvektors x
%--------------------
% Eingabeparameter :
% A : Matrix der Dimension n * n
% lambda0 : Approximation des Eigenwertes lambda von A
% x0 : Startvektor zur inversen Vektoriteration
% N : Anzahl der Iterationsschritte
% Ausgabeparameter :
%--------------------
% lambda : neu berechnete Approximation des Eigenwertes von A
% x : neu berechnete Approximation des zug . Eigenvektors
%--------------------
% Frederic Weidling, 25.01.16

x=x0(:)/norm(x0);
[L,U,P]=lu_pivot(A-lambda0*eye(size(A)));

for iii=1:N
    x = L\(P*x);
    x = U\x;
    x = x/norm(x);
end

lambda = x'*A*x;

