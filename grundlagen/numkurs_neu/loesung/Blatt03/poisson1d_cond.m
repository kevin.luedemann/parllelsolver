% Poisson1d_cond
%
% erstellt die Matrizen, die sich aus der Diskretisierung des Poisson
% Problems in 1d ergeben und berechnet die zugehörige Konditionszahl, die
% ermittelten Werte werden geplottet und der vorhergesagte quadratische
% Verlauf wird ersichtlich
%
% Frederic Weidling, 10.11.15

n=[5:5:30];

kappa_2=zeros(size(n));
for iii=1:length(n)
    a=-1/(n(iii)+1)^2*ones(1,n(iii)-1);
    b=2/(n(iii)+1)^2*ones(1,n(iii));
    P=tridiagonal(a,b,a);
    
    kappa_2(iii)=cond(P);
end

plot(n,kappa_2)
title('Konditionszahl der 1d Poissonmatrix')
xlabel('Matrixgröße n')
ylabel('\kappa_2(n)')