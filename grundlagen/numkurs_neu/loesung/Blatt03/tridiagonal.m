function A=tridiagonal(a,b,c)

% tridiagonal.m
%
% baut eine tridiagonal Matrix aus drei gegebenen Vektoren 
% Input: Vektoren a, b,c
% Output: Matrix A
%
% Frederic Weidling 23.10.15

% Überprüfung ob die Längen der Vektoren konsistent sind
n=[length(a),length(b),length(c)];
if (n(1)~=n(3) || n(1)+1~=n(2))
    error('Vektoren haben nicht zueinander passende Größen')
end

% Sicherstellen, dass es sich bei allen eingegeben Vektoren um Zeilenvektoren handelt
% (Mit einer leichten Anpassung könnte man das genauso für Spaltenvektoren machen)
if ~(isrow(a))
    a=a.';
end
if ~(isrow(b))
    b=b.';
end
if ~(isrow(c))
    c=c.';
end

% a und c auf die richtige Länge bringen um spdiags verwenden zu können
% (der ergänzte Eintrag ist beliebig) 
a=[a,0];
c=[0,c];

% Anweden von spdiags und Ausgabe der vollen Matrix
A=spdiags([a;b;c]',[-1,0,1],n(2),n(2));
A=full(A);
