% mandelbrot.m
% Numerische Näherung der Mandelbrotmenge
%Frederic Weidling, 4.1.2016

[X,Y]=meshgrid(-2:0.01:1,-1:0.01:1);
C=X+1i*Y;
Z=zeros(size(C));

for iii=1:numel(C)
        z=0;
        for jjj=1:100
                z=z^2+C(iii);
        end
        if abs(z)<=100
                Z(iii)=1;
        end
end

